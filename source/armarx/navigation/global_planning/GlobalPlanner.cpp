#include "GlobalPlanner.h"

#include <armarx/navigation/core/constants.h>
#include <armarx/navigation/core/types.h>

namespace armarx::navigation::global_planning
{

    GlobalPlanner::GlobalPlanner(const core::Scene& context) : scene(context)
    {
    }

} // namespace armarx::navigation::global_planning
