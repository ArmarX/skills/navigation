/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::global_planning
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE Navigation::ArmarXLibraries::global_planning

#define ARMARX_BOOST_TEST

#include <cmath>
#include <cstddef>
#include <iostream>

#include <boost/test/tools/old/interface.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <SimoxUtility/math/convert/mat3f_to_rpy.h>

#include <armarx/navigation/Test.h>
#include <armarx/navigation/core/basic_types.h>
#include <armarx/navigation/global_planning/optimization/OrientationOptimizer.h>

BOOST_AUTO_TEST_CASE(testOrientationOptimizationRotatedGoalMinimalPrior)
{
    namespace nav = armarx::navigation;
    namespace gp = armarx::navigation::global_planning;

    nav::core::Path path;
    path.emplace_back(nav::core::Pose::Identity());
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 500});
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 1000});
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 1500});
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 2000} *
                      Eigen::AngleAxis(M_PI_2f32, Eigen::Vector3f::UnitZ()));

    const auto trajectory = nav::core::Trajectory::FromPath(path, 100);

    gp::OrientationOptimizer::Params params{
        .priorWeight = 1.F, .smoothnessWeight = 0.F, .smoothnessWeightStartGoal = 0.0F};


    gp::OrientationOptimizer optimizer(trajectory, params);

    const gp::OrientationOptimizer::OptimizationResult result = optimizer.optimize();
    BOOST_REQUIRE(result);
}


BOOST_AUTO_TEST_CASE(testOrientationOptimizationRotatedGoalMinimalSmoothness)
{
    namespace nav = armarx::navigation;
    namespace gp = armarx::navigation::global_planning;

    nav::core::Path path;
    path.emplace_back(nav::core::Pose::Identity());
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 500});
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 1000});
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 1500});
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 2000} *
                      Eigen::AngleAxis(M_PI_2f32, Eigen::Vector3f::UnitZ()));

    const auto trajectory = nav::core::Trajectory::FromPath(path, 100);

    gp::OrientationOptimizer::Params params{
        .priorWeight = 0.F, .smoothnessWeight = 1.F, .smoothnessWeightStartGoal = 0.0F};


    gp::OrientationOptimizer optimizer(trajectory, params);

    const gp::OrientationOptimizer::OptimizationResult result = optimizer.optimize();
    BOOST_REQUIRE(result);
}


BOOST_AUTO_TEST_CASE(testOrientationOptimizationRotatedGoalMinimalSmoothnessStartGoal)
{
    namespace nav = armarx::navigation;
    namespace gp = armarx::navigation::global_planning;

    nav::core::Path path;
    path.emplace_back(nav::core::Pose::Identity());
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 500});
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 1000});
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 1500});
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 2000} *
                      Eigen::AngleAxis(M_PI_2f32, Eigen::Vector3f::UnitZ()));

    const auto trajectory = nav::core::Trajectory::FromPath(path, 100);

    gp::OrientationOptimizer::Params params{
        .priorWeight = 0.F, .smoothnessWeight = 0.F, .smoothnessWeightStartGoal = 1.0F};


    gp::OrientationOptimizer optimizer(trajectory, params);

    const gp::OrientationOptimizer::OptimizationResult result = optimizer.optimize();
    BOOST_REQUIRE(result);
}


BOOST_AUTO_TEST_CASE(testOrientationOptimizationRotatedGoal)
{
    namespace nav = armarx::navigation;
    namespace gp = armarx::navigation::global_planning;

    nav::core::Path path;
    path.emplace_back(nav::core::Pose::Identity());
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 500});
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 1000});
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 1500});
    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 2000} *
                      Eigen::AngleAxis(M_PI_2f32, Eigen::Vector3f::UnitZ()));

    const auto trajectory = nav::core::Trajectory::FromPath(path, 100);

    gp::OrientationOptimizer::Params params{};


    gp::OrientationOptimizer optimizer(trajectory, params);

    const gp::OrientationOptimizer::OptimizationResult result = optimizer.optimize();

    // # of points should be unchanged
    BOOST_REQUIRE_EQUAL(result.trajectory.value().poses().size(), trajectory.poses().size());

    // avoid an underflow in the for loop below
    BOOST_REQUIRE_GE(trajectory.poses().size(), 2);

    // loop over intermediate points
    for (std::size_t i = 1; i < (result.trajectory.value().poses().size() - 2); i++)
    {
        const auto rpy =
            simox::math::mat3f_to_rpy(result.trajectory.value().poses().at(i).linear());
        const auto yaw = rpy.z();

        // The orientation should be changed compared to the unoptimized version.
        // As the last pose is rotated by 90°, the intermediate poses should at least be
        // changed gentlty into this direction.
        BOOST_CHECK_GT(yaw, 0.1);
    }
}


BOOST_AUTO_TEST_CASE(testOrientationOptimizationCircular)
{
    namespace nav = armarx::navigation;
    namespace gp = armarx::navigation::global_planning;

    nav::core::Path path;
    path.emplace_back(nav::core::Pose::Identity());
    for (int i = 0; i < 20; i++)
    {
        path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 500 * i} *
                          Eigen::AngleAxis(M_PI_2f32 * i, Eigen::Vector3f::UnitZ()));
    }

    path.emplace_back(Eigen::Translation3f{Eigen::Vector3f::UnitX() * 500 * 20});


    const auto trajectory = nav::core::Trajectory::FromPath(path, 100);

    // Here we don't care about the orientatation prior.
    // The orientation of the intermediate points should converge to 0 (same direction as start and goal)
    gp::OrientationOptimizer::Params params{.priorWeight = 0.F};


    gp::OrientationOptimizer optimizer(trajectory, params);

    const gp::OrientationOptimizer::OptimizationResult result = optimizer.optimize();

    // # of points should be unchanged
    BOOST_REQUIRE_EQUAL(result.trajectory.value().poses().size(), trajectory.poses().size());

    // avoid an underflow in the for loop below
    BOOST_REQUIRE_GE(trajectory.poses().size(), 2);

    // loop over intermediate points
    for (std::size_t i = 0; i < (result.trajectory.value().poses().size() - 2); i++)
    {
        const auto rpy =
            simox::math::mat3f_to_rpy(result.trajectory.value().poses().at(i).linear());
        const auto yaw = rpy.z();

        // The orientation should be changed compared to the unoptimized version.
        // As the last pose is oriented the same way as the start, the intermediate poses should
        // have the same orientation. => yaw ~= 0
        BOOST_CHECK_LT(std::abs(yaw), 0.01);
    }
}
