/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <armarx/navigation/core/types.h>
#include <armarx/navigation/global_planning/GlobalPlanner.h>

namespace armarx::navigation::global_planning
{

    /**
         * @brief Parameters for Point2Point
         *
         */
    struct Point2PointParams : public GlobalPlannerParams
    {
        bool includeStartPose{true};

        float velocity{400};

        Algorithms algorithm() const override;
        aron::data::DictPtr toAron() const override;

        static Point2PointParams FromAron(const aron::data::DictPtr& dict);
    };

    /**
    * @class Point2Point
    * @ingroup Library-GlobalPlanner
    *
    */
    class Point2Point : virtual public GlobalPlanner
    {
    public:
        using Params = Point2PointParams;

        Point2Point(const Params& params, const core::Scene& ctx);

        std::optional<GlobalPlannerResult> plan(const core::Pose& goal) override;
        std::optional<GlobalPlannerResult> plan(const core::Pose& start,
                                                const core::Pose& goal) override;

    protected:
        const Params params;

    private:
    };
} // namespace armarx::navigation::global_planning
