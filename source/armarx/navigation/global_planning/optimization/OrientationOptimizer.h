/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <optional>

#include <armarx/navigation/core/Trajectory.h>

namespace armarx::navigation::global_planning::optimization
{

    struct OrientationOptimizerParams
    {
        float priorWeight{0.25F};
        float smoothnessWeight{0.4F};
        float smoothnessWeightStartGoal{1.F};

        float priorStartWeight{1.F};
        float priorEndWeight{1.F};

        float startGoalDistanceThreshold{500};
    };

    class OrientationOptimizer
    {
    public:
        using Params = OrientationOptimizerParams;

        OrientationOptimizer(const core::Trajectory& trajectory, const Params& params);


        struct OptimizationResult
        {
            std::optional<core::Trajectory> trajectory;

            operator bool() const noexcept
            {
                return trajectory.has_value();
            }
        };


        OptimizationResult optimize();

    protected:
    private:
        core::Trajectory trajectory;

        const Params params;
    };
} // namespace armarx::navigation::global_planning::optimization

// FIXME remove
namespace armarx::navigation::global_planning
{
    using namespace armarx::navigation::global_planning::optimization;
}
