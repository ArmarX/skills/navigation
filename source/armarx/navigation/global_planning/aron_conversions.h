#pragma once

namespace armarx::navigation::global_planning
{
    struct GlobalPlannerParams;
    struct Point2PointParams;
    struct AStarParams;
    struct SPFAParams;

    namespace arondto
    {
        struct GlobalPlannerParams;
        struct Point2PointParams;
        struct AStarParams;
        struct SPFAParams;

    } // namespace arondto

} // namespace armarx::navigation::global_planning

namespace armarx::navigation::global_planning::aron_conv
{
    void toAron(arondto::GlobalPlannerParams& dto, const GlobalPlannerParams& bo);
    void fromAron(const arondto::GlobalPlannerParams& dto, GlobalPlannerParams& bo);

    void toAron(arondto::Point2PointParams& dto, const Point2PointParams& bo);
    void fromAron(const arondto::Point2PointParams& dto, Point2PointParams& bo);

    void toAron(arondto::AStarParams& dto, const AStarParams& bo);
    void fromAron(const arondto::AStarParams& dto, AStarParams& bo);

    void toAron(arondto::SPFAParams& dto, const SPFAParams& bo);
    void fromAron(const arondto::SPFAParams& dto, SPFAParams& bo);

} // namespace armarx::navigation::global_planning::aron_conv
