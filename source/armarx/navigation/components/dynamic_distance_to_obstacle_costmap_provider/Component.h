/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    navigation::ArmarXObjects::dynamic_distance_to_obstacle_costmap_provider
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/Component.h>

#include <RobotAPI/libraries/armem/client/forward_declarations.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/RobotReader.h>
#include <RobotAPI/libraries/armem_vision/client/laser_scanner_features/Reader.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/armem/client.h>

#include <armarx/navigation/memory/client/costmap/Reader.h>
#include <armarx/navigation/memory/client/costmap/Writer.h>
#include <armarx/navigation/components/dynamic_distance_to_obstacle_costmap_provider/ComponentInterface.h>


namespace armarx::navigation::components::dynamic_distance_to_obstacle_costmap_provider
{

    class Component :
        virtual public armarx::Component,
        virtual public armarx::navigation::components::
            dynamic_distance_to_obstacle_costmap_provider::ComponentInterface,
        // , virtual public armarx::DebugObserverComponentPluginUser
        // , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armem::ListeningClientPluginUser
    {
    public:
        Component();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();


    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


        void updateCostmap();

        bool readStaticCostmap();

        void drawCostmap(const armarx::navigation::algorithms::Costmap& costmap,
                         const std::string& name,
                         float heightOffset);

        algorithms::Costmap
        computeNavigationCostmap(const algorithms::Costmap& obstacleDistancesCostmap);


        /* (Requires armarx::LightweightRemoteGuiComponentPluginUser.)
        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;
        */


    private:
        // Private methods go here.

        // Forward declare `Properties` if you used it before its defined.
        // struct Properties;

        /* (Requires the armarx::ArVizComponentPluginUser.)
        /// Draw some boxes in ArViz.
        void drawBoxes(const Properties& p, viz::Client& arviz);
        */


        static const std::string defaultName;


        // Private member variables go here.


        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            // std::string boxLayerName = "boxes";
            struct
            {
                std::string providerName = "navigator";
                std::string name = "distance_to_obstacles";
            } staticCostmap;

            struct
            {
                std::string providerName = "LaserScannerFeatureExtraction";
                std::string name = ""; // all
            } laserScannerFeatures;

            struct
            {
                std::string name = "Armar6";
            } robot;

            int updatePeriodMs = 100;
        };
        Properties properties;
        /* Use a mutex if you access variables from different threads
         * (e.g. ice functions and RemoteGui_update()).
        std::mutex propertiesMutex;
        */


        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::LineEdit boxLayerName;
            armarx::RemoteGui::Client::IntSpinBox numBoxes;

            armarx::RemoteGui::Client::Button drawBoxes;
        };
        RemoteGuiTab tab;
        */


        /* (Requires the armarx::ArVizComponentPluginUser.)
         * When used from different threads, an ArViz client needs to be synchronized.
        /// Protects the arviz client inherited from the ArViz plugin.
        std::mutex arvizMutex;
        */

        armem::robot_state::RobotReader robotReader;

        memory::client::costmap::Reader costmapReader;
        memory::client::costmap::Writer costmapWriter;

        armem::vision::laser_scanner_features::client::Reader laserScannerFeaturesReader;

        std::optional<algorithms::Costmap> staticCostmap;

        PeriodicTask<Component>::pointer_type updateCostmapTask;
    };

} // namespace armarx::navigation::components::dynamic_distance_to_obstacle_costmap_provider
