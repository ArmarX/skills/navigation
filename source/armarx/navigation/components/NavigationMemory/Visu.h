/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::NavigationMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>
#include <vector>

#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/libraries/armem/core/forward_declarations.h>


namespace armarx::navigation::graph
{
    class GraphVisu;
}

namespace armarx::navigation::memory
{

    class Visu
    {
    public:
        Visu(viz::Client arviz,
             const armem::server::wm::CoreSegment& locSegment,
             const armem::server::wm::CoreSegment& graphSegment);
        ~Visu();


        void drawLocations(std::vector<viz::Layer>& layers, bool enabled);
        void drawGraphs(std::vector<viz::Layer>& layers, bool enabled);


    public:
        viz::Client arviz;

        const armem::server::wm::CoreSegment& locSegment;
        const armem::server::wm::CoreSegment& graphSegment;

        std::unique_ptr<navigation::graph::GraphVisu> visu;
    };


} // namespace armarx::navigation::memory
