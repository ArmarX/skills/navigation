armarx_add_component(navigation_memory
    DEPENDENCIES
        # ArmarXCore
        ArmarXCore
        ## ArmarXCoreComponentPlugins  # For DebugObserver plugin.
        # ArmarXGui
        ArmarXGuiComponentPlugins  # For RemoteGui plugin.
        # RobotAPI
        RobotAPICore
        armem_server
        ## RobotAPIInterfaces
        RobotAPIComponentPlugins  # For ArViz and other plugins.

        # This project
        ## ${PROJECT_NAME}Interfaces  # For ice interfaces from this package.
        # This component
        ## NavigationMemoryInterfaces  # If you defined a component ice interface above.
        armarx_navigation::graph
        armarx_navigation::location

    SOURCES
        NavigationMemory.cpp
        Visu.cpp

    HEADERS
        NavigationMemory.h
        Visu.h
)
