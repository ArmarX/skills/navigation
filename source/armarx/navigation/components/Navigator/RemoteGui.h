/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include "ArmarXGui/libraries/RemoteGui/Client/Widgets.h"
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/Client/Tab.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

namespace armarx::navigation::components
{
    class Navigator;

    class NavigatorRemoteGui
    {
    public:
        NavigatorRemoteGui(const RemoteGuiInterfacePrx& remoteGui, Navigator& navigator);

        ~NavigatorRemoteGui();

        void shutdown();

        void enable();
        void disable();

        void reset();

    private:
        std::mutex mtx;

        std::mutex handleEventsMtx;
        RemoteGuiInterfacePrx remoteGui;

        RunningTask<NavigatorRemoteGui>::pointer_type runningTask;

        Navigator& navigator;

        RemoteGui::TabProxy tabPrx;

        void createRemoteGuiTab();

        // thread function
        void run();

        void handleEvents(RemoteGui::TabProxy& tab);

        const std::string REMOTE_GUI_TAB_MAME = "Navigator";

        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::HBoxLayout hbox;

            struct
            {
                armarx::RemoteGui::Client::GroupBox group;

                armarx::RemoteGui::Client::FloatSpinBox targetPoseX;
                armarx::RemoteGui::Client::FloatSpinBox targetPoseY;
                armarx::RemoteGui::Client::FloatSpinBox targetPoseZ;
                armarx::RemoteGui::Client::FloatSpinBox targetPoseRoll;
                armarx::RemoteGui::Client::FloatSpinBox targetPosePitch;
                armarx::RemoteGui::Client::FloatSpinBox targetPoseYaw;
            } targetPoseGroup;

            struct
            {
                armarx::RemoteGui::Client::GroupBox group;

                armarx::RemoteGui::Client::Button stopAllButton;

                // Control buttons for this navigator client
                armarx::RemoteGui::Client::Button moveToButton;
                armarx::RemoteGui::Client::Button pauseButton;
                armarx::RemoteGui::Client::Button continueButton;
                armarx::RemoteGui::Client::Button stopButton;

            } controlGroup;
        };
        RemoteGuiTab tab;
    };

} // namespace armarx::navigation::components
