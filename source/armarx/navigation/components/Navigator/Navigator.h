/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::Navigator
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <optional>
#include <string>
#include <vector>

#include <Eigen/Core>

#include <Ice/Object.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/util/CPPUtility/TripleBuffer.h>

#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/plugins.h>
#include <RobotAPI/libraries/armem/client/plugins/ReaderWriterPlugin.h>
#include <RobotAPI/libraries/armem_robot/types.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h>
#include <RobotAPI/libraries/armem_vision/client/occupancy_grid/Reader.h>

#include <armarx/control/client/ComponentPlugin.h>
#include <armarx/navigation/client/ice/NavigatorInterface.h>
#include <armarx/navigation/components/Navigator/RemoteGui.h>
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/memory/client/costmap/Writer.h>
#include <armarx/navigation/memory/client/graph/Reader.h>
#include <armarx/navigation/memory/client/parameterization/Reader.h>
#include <armarx/navigation/memory/client/stack_result/Writer.h>
#include <armarx/navigation/server/Navigator.h>
#include <armarx/navigation/server/event_publishing/MemoryPublisher.h>
#include <armarx/navigation/server/execution/PlatformControllerExecutor.h>
#include <armarx/navigation/server/introspection/ArvizIntrospector.h>
#include <armarx/navigation/server/introspection/MemoryIntrospector.h>
#include <armarx/navigation/server/parameterization/MemoryParameterizationService.h>
#include <armarx/navigation/server/scene_provider/SceneProvider.h>


namespace armarx::navigation::components
{

    /**
     * @defgroup Component-Navigator Navigator
     * @ingroup armarx_navigation-Components
     * A description of the component Navigator.
     *
     * @class Navigator
     * @ingroup Component-Navigator
     * @brief Brief description of class Navigator.
     *
     * Detailed description of class Navigator.
     */
    class Navigator :
        virtual public Component,
        virtual public client::NavigatorInterface,
        virtual public ObjectPoseClientPluginUser,
        virtual public ArVizComponentPluginUser,
        virtual public armarx::control::client::ComponentPluginUser
    // virtual public armem::ListeningClientPluginUser
    {

    public:
        Navigator();
        ~Navigator() override;

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        void createConfig(const aron::data::dto::DictPtr& stackConfig,
                          const std::string& callerId,
                          const Ice::Current& c = Ice::emptyCurrent) override;

        void moveTo(const std::vector<Eigen::Matrix4f>& waypoints,
                    const std::string& navigationMode,
                    const std::string& callerId,
                    const Ice::Current& c = Ice::emptyCurrent) override;

        void moveTo2(const client::detail::Waypoints& waypoints,
                     const std::string& navigationMode,
                     const std::string& callerId,
                     const Ice::Current& c = Ice::emptyCurrent) override;

        void moveTowards(const Eigen::Vector3f& direction,
                         const std::string& navigationMode,
                         const std::string& callerId,
                         const Ice::Current& c = Ice::emptyCurrent) override;

        void pause(const std::string& callerId, const Ice::Current& c = Ice::emptyCurrent) override;

        void resume(const std::string& callerId,
                    const Ice::Current& c = Ice::emptyCurrent) override;

        void stop(const std::string& callerId, const Ice::Current& c = Ice::emptyCurrent) override;

        void stopAll(const Ice::Current& c = Ice::emptyCurrent) override;

        bool isPaused(const std::string& callerId,
                      const Ice::Current& c = Ice::emptyCurrent) override;

        bool isStopped(const std::string& callerId,
                       const Ice::Current& c = Ice::emptyCurrent) override;

        const core::Scene& scene() const;

    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        void onReconnectComponent();
        void initializeScene();

        // core::StaticScene staticScene();

        // [[nodiscard]] VirtualRobot::RobotPtr getRobot();
        // void updateRobot();

        server::Navigator* activeNavigator();

    private:
        void visualizeSPFA();

        RemoteGuiInterfacePrx remoteGui;

        // core::Scene scene;

        std::optional<server::PlatformControllerExecutor> executor;
        std::optional<server::ArvizIntrospector> introspector;
        // std::optional<server::MemoryIntrospector> memoryIntrospector;
        std::optional<server::scene_provider::SceneProvider> sceneProvider;

        std::unordered_map<std::string, server::Navigator> navigators;

        std::mutex propertiesMutex;

        // TODO maybe as optional, but requires some effort
        std::unique_ptr<NavigatorRemoteGui> navRemoteGui;


        // unique_ptr to avoid dangling refs
        std::vector<std::unique_ptr<server::MemoryIntrospector>> memoryIntrospectors;

        // `navigation` memory reader and writer
        armem::client::plugins::ReaderWriterPlugin<memory::client::param::Reader>*
            parameterizationReaderPlugin = nullptr;
        armem::client::plugins::ReaderWriterPlugin<memory::client::param::Writer>*
            parameterizationWriterPlugin = nullptr;
        armem::client::plugins::ReaderWriterPlugin<memory::client::events::Writer>*
            eventsWriterPlugin = nullptr;
        armem::client::plugins::ReaderWriterPlugin<memory::client::stack_result::Writer>*
            resultsWriterPlugin = nullptr;
        armem::client::plugins::ReaderWriterPlugin<memory::client::graph::Reader>*
            graphReaderPlugin = nullptr;
        armem::client::plugins::ReaderWriterPlugin<memory::client::costmap::Reader>*
            costmapReaderPlugin = nullptr;
        armem::client::plugins::ReaderWriterPlugin<memory::client::costmap::Writer>*
            costmapWriterPlugin = nullptr;
        // armem::vision::occupancy_grid::client::Reader occupancyGridReader;

        // `robot_state` memory reader and writer
        std::optional<armem::robot::RobotDescription> robotDescription;
        armem::client::plugins::ReaderWriterPlugin<armem::robot_state::VirtualRobotReader>*
            virtualRobotReaderPlugin = nullptr;


        server::MemoryParameterizationService parameterizationService;
        // server::MemoryPublisher publisher;

        // key is `callerId`
        std::unordered_map<std::string, std::unique_ptr<server::MemoryPublisher>> memoryPublishers;

        struct Parameters
        {
            float occupiedGridThreshold{0.55F};
        };

        Parameters params;
    };
} // namespace armarx::navigation::components
