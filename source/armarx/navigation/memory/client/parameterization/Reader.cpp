#include "Reader.h"

#include <armarx/navigation/memory/constants.h>

namespace armarx::navigation::memory::client::param
{
    std::string
    Reader::propertyPrefix() const
    {
        return "mem.nav.param.";
    }

    Reader::Properties
    Reader::defaultProperties() const
    {
        return Properties{.memoryName = memory::constants::NavigationMemoryName,
                          .coreSegmentName = memory::constants::ParameterizationCoreSegmentName};
    }

} // namespace armarx::navigation::memory::client::param
