#include "Writer.h"

#include <RobotAPI/libraries/aron/core/data/variant/All.h>

#include <armarx/navigation/core/constants.h>
#include <armarx/navigation/memory/constants.h>


namespace armarx::navigation::memory::client::param
{
    bool
    Writer::store(const std::unordered_map<core::StackLayer, aron::data::DictPtr>& stack,
                  const std::string& clientID,
                  const armarx::core::time::DateTime& timestamp)
    {
        ARMARX_CHECK(not stack.empty());

        armem::Commit commit;

        for (const auto& [layer, data] : stack)
        {
            const std::string layerName = core::StackLayerNames.to_name(layer);

            armem::EntityUpdate update;
            update.entityID = armem::MemoryID()
                                  .withMemoryName(properties().memoryName)
                                  .withCoreSegmentName(properties().coreSegmentName)
                                  .withProviderSegmentName(clientID)
                                  .withEntityName(layerName)
                                  .withTimestamp(timestamp);
            update.timeCreated = timestamp;
            update.instancesData = {data};
            commit.add(update);
        }

        std::lock_guard g{memoryWriterMutex()};
        armem::CommitResult updateResult = memoryWriter().commit(commit);

        ARMARX_DEBUG << updateResult;

        if (not updateResult.allSuccess())
        {
            ARMARX_ERROR << updateResult.allErrorMessages();
        }
        return updateResult.allSuccess();
    }

    std::string
    Writer::propertyPrefix() const
    {
        return "mem.nav.param.";
    }

    Writer::Properties
    Writer::defaultProperties() const
    {
        return Properties{
            .memoryName = memory::constants::NavigationMemoryName,
            .coreSegmentName = memory::constants::ParameterizationCoreSegmentName,
            .providerName = "" // clientId
        };
    }

} // namespace armarx::navigation::memory::client::param
