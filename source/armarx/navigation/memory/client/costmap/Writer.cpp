#include "Writer.h"

#include <armarx/navigation/algorithms/aron/Costmap.aron.generated.h>
#include <armarx/navigation/algorithms/aron_conversions.h>
#include <armarx/navigation/memory/constants.h>


namespace armarx::navigation::memory::client::costmap
{
    Writer::~Writer() = default;

    bool
    Writer::store(const algorithms::Costmap& grid,
                  const std::string& name,
                  const std::string& providerName,
                  const armem::Time& timestamp)
    {
        std::lock_guard g{memoryWriterMutex()};

        const auto result = memoryWriter().addSegment(memory::constants::CostmapCoreSegmentName, providerName);

        if (not result.success)
        {
            ARMARX_ERROR << result.errorMessage;

            // TODO(fabian.reister): message
            return false;
        }

        const auto providerId = armem::MemoryID(result.segmentID);
        const auto entityID = providerId.withEntityName(name).withTimestamp(timestamp);

        armem::EntityUpdate update;
        update.entityID = entityID;

        auto aronCostmap = toAron(grid);

        update.instancesData = {aronCostmap};
        update.timeCreated = timestamp;

        ARMARX_DEBUG << "Committing " << update << " at time " << timestamp;
        armem::EntityUpdateResult updateResult = memoryWriter().commit(update);

        ARMARX_DEBUG << updateResult;

        if (not updateResult.success)
        {
            ARMARX_ERROR << updateResult.errorMessage;
        }

        return updateResult.success;
    }

    std::string
    Writer::propertyPrefix() const
    {
        return "mem.nav.costmap.";
    }

    armarx::armem::client::util::SimpleWriterBase::SimpleWriterBase::Properties
    Writer::defaultProperties() const
    {
        return SimpleWriterBase::Properties{.memoryName = memory::constants::NavigationMemoryName,
                                            .coreSegmentName =
                                                memory::constants::CostmapCoreSegmentName};
    }

} // namespace armarx::navigation::memory::client::costmap
