/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/armem/client/util/SimpleWriterBase.h>
#include <RobotAPI/libraries/armem/core/Commit.h>

#include <armarx/navigation/core/constants.h>
#include <armarx/navigation/core/events.h>
#include <armarx/navigation/core/types.h>


namespace armarx::navigation::memory::client::events
{

    class Writer : virtual public armem::client::util::SimpleWriterBase
    {
    public:
        using armem::client::util::SimpleWriterBase::SimpleWriterBase;

        bool store(const core::GoalReachedEvent& event, const std::string& clientID);
        bool store(const core::WaypointReachedEvent& event, const std::string& clientID);
        bool store(const core::InternalErrorEvent& event, const std::string& clientID);
        bool store(const core::UserAbortTriggeredEvent& event, const std::string& clientID);
        bool store(const core::GlobalPlanningFailedEvent& event, const std::string& clientID);
        bool store(const core::MovementStartedEvent& event, const std::string& clientID);
        bool store(const core::SafetyThrottlingTriggeredEvent& event, const std::string& clientID);
        bool store(const core::SafetyStopTriggeredEvent& event, const std::string& clientID);


        std::string propertyPrefix() const override;
        Properties defaultProperties() const override;

    protected:
    private:
        bool store(const armem::Commit& commit);
        bool store(const armem::EntityUpdate& commit);

        template <typename AronEventT, typename EventT>
        bool
        storeImpl(const EventT& event, const std::string& eventName, const std::string& clientID);
    };
} // namespace armarx::navigation::memory::client::events
