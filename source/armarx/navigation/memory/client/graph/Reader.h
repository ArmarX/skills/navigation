/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/armem/client/util/SimpleReaderBase.h>

#include <armarx/navigation/core/Graph.h>
#include <armarx/navigation/core/aron/Location.aron.generated.h>
#include <armarx/navigation/core/types.h>

namespace armarx::navigation::memory::client::graph
{

    class Reader : virtual public armem::client::util::SimpleReaderBase
    {
    public:
        using armem::client::util::SimpleReaderBase::SimpleReaderBase;

        std::string propertyPrefix() const override;
        Properties defaultProperties() const override;

        std::map<std::string, core::Pose> locations();

        std::vector<armarx::navigation::core::Graph> graphs();

        core::Pose resolveLocationId(const std::string& locationId);

        void connect() override;


    protected:
        std::map<armem::MemoryID, location::arondto::Location> queryLocations();

        armem::wm::CoreSegment allLocations();
        armem::wm::CoreSegment allGraphs();

    private:
        armem::client::Reader memoryReaderLocations;
        armem::client::Reader memoryReaderGraphs;
    };

} // namespace armarx::navigation::memory::client::graph
