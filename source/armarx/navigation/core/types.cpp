#include "types.h"

namespace armarx::navigation::core
{
    core::Pose
    Twist::poseDiff(const float dt) const
    {
        return core::Pose(Eigen::Translation3f(linear * dt)) *
               core::Pose(Eigen::AngleAxisf(angular.norm() * dt, angular.normalized()));
    }

    Twist
    Twist::Zero()
    {
        static const core::Twist zero{Eigen::Vector3f::Zero(), Eigen::Vector3f::Zero()};
        return zero;
    }
} // namespace armarx::navigation::core
