#include "aron_conversions.h"

#include <range/v3/range/conversion.hpp>
#include <range/v3/view/transform.hpp>

#include <RobotAPI/libraries/aron/common/aron_conversions/core.h>
#include <RobotAPI/libraries/aron/common/aron_conversions.h>
#include <RobotAPI/libraries/core/Trajectory.h>

#include <armarx/navigation/core/Trajectory.h>
#include <armarx/navigation/core/aron/Trajectory.aron.generated.h>
#include <armarx/navigation/core/types.h>

namespace armarx::navigation::core
{


    void
    toAron(arondto::TrajectoryPoint& dto, const TrajectoryPoint& bo)
    {
        dto.pose = bo.waypoint.pose.matrix();
        dto.velocity = bo.velocity;
    }

    void
    fromAron(const arondto::TrajectoryPoint& dto, TrajectoryPoint& bo)
    {
        bo.waypoint.pose = core::Pose(dto.pose);
        bo.velocity = dto.velocity;
    }

    void
    toAron(arondto::Trajectory& dto, const Trajectory& bo)
    {
        dto.points = bo.points() |
                     ranges::views::transform(
                         [](const TrajectoryPoint& boTp) -> arondto::TrajectoryPoint
                         {
                             arondto::TrajectoryPoint dtoTp;
                             toAron(dtoTp, boTp);
                             return dtoTp;
                         }) |
                     ranges::to_vector;
    }

    void
    fromAron(const arondto::Trajectory& dto, Trajectory& bo)
    {
        bo = Trajectory(
            dto.points |
            ranges::views::
                transform( //static_cast<TrajectoryPoint (*)(const arondto::TrajectoryPoint&)>(&fromAron)
                    [](const arondto::TrajectoryPoint& dto) -> TrajectoryPoint
                    {
                        TrajectoryPoint bo;
                        fromAron(dto, bo);
                        return bo;
                    }) |
            ranges::to_vector);
    }

    void
    toAron(arondto::Twist& dto, const Twist& bo)
    {
        dto.linear = bo.linear;
        dto.angular = bo.angular;
    }

    void
    fromAron(const arondto::Twist& dto, Twist& bo)
    {
        bo.linear = dto.linear;
        bo.angular = dto.angular;
    }

    void
    toAron(armarx::navigation::core::arondto::PIDParams& dto,
           const armarx::navigation::core::PIDParams& bo)
    {
        aron::toAron(dto.Kp, bo.Kp);
        aron::toAron(dto.Ki, bo.Ki);
        aron::toAron(dto.Kd, bo.Kd);
    }


    void
    fromAron(const armarx::navigation::core::arondto::PIDParams& dto,
             armarx::navigation::core::PIDParams& bo)
    {
        aron::fromAron(dto.Kp, bo.Kp);
        aron::fromAron(dto.Ki, bo.Ki);
        aron::fromAron(dto.Kd, bo.Kd);
    }

    void
    toAron(armarx::navigation::core::arondto::TwistLimits& dto,
           const armarx::navigation::core::TwistLimits& bo)
    {
        aron::toAron(dto.linear, bo.linear);
        aron::toAron(dto.angular, bo.angular);
    }

    void
    fromAron(const armarx::navigation::core::arondto::TwistLimits& dto,
             armarx::navigation::core::TwistLimits& bo)
    {
        aron::fromAron(dto.linear, bo.linear);
        aron::fromAron(dto.angular, bo.angular);
    }

    void
    toAron(armarx::navigation::core::arondto::GoalReachedEvent& dto,
           const armarx::navigation::core::GoalReachedEvent& bo)
    {
        aron::toAron(dto.pose, bo.pose);
    }

    void
    fromAron(const armarx::navigation::core::arondto::GoalReachedEvent& dto,
             armarx::navigation::core::GoalReachedEvent& bo)
    {
        aron::fromAron(dto.pose, bo.pose.matrix());
    }

    void
    fromAron(const armarx::navigation::core::arondto::GlobalPlanningFailedEvent& dto,
             armarx::navigation::core::GlobalPlanningFailedEvent& bo)
    {
        aron::fromAron(dto.message, bo.message);
    }

    void
    toAron(armarx::navigation::core::arondto::GlobalPlanningFailedEvent& dto,
           const armarx::navigation::core::GlobalPlanningFailedEvent& bo)
    {
        aron::toAron(dto.message, bo.message);
    }

    void
    toAron(armarx::navigation::core::arondto::MovementStartedEvent& dto,
           const armarx::navigation::core::MovementStartedEvent& bo)
    {
        aron::toAron(dto.startPose, bo.startPose.matrix());
    }

    void
    fromAron(const armarx::navigation::core::arondto::MovementStartedEvent& dto,
             armarx::navigation::core::MovementStartedEvent& bo)
    {
        aron::fromAron(dto.startPose, bo.startPose.matrix());
    }

    void
    toAron(armarx::navigation::core::arondto::WaypointReachedEvent& dto,
           const armarx::navigation::core::WaypointReachedEvent& bo)
    {
        aron::toAron(dto.pose, bo.pose);
        aron::toAron(dto.waypointId, bo.waypointId);
    }

    void
    fromAron(const armarx::navigation::core::arondto::WaypointReachedEvent& dto,
             armarx::navigation::core::WaypointReachedEvent& bo)
    {
        aron::fromAron(dto.pose, bo.pose.matrix());
        aron::fromAron(dto.waypointId, bo.waypointId);
    }

    void
    toAron(armarx::navigation::core::arondto::InternalErrorEvent& dto,
           const armarx::navigation::core::InternalErrorEvent& bo)
    {
        aron::toAron(dto.pose, bo.pose);
        aron::toAron(dto.message, bo.message);
    }

    void
    fromAron(const armarx::navigation::core::arondto::InternalErrorEvent& dto,
             armarx::navigation::core::InternalErrorEvent& bo)
    {
        aron::fromAron(dto.pose, bo.pose.matrix());
        aron::fromAron(dto.message, bo.message);
    }

    void
    toAron(armarx::navigation::core::arondto::UserAbortTriggeredEvent& dto,
           const armarx::navigation::core::UserAbortTriggeredEvent& bo)
    {
        aron::toAron(dto.pose, bo.pose.matrix());
    }

    void
    fromAron(const armarx::navigation::core::arondto::UserAbortTriggeredEvent& dto,
             armarx::navigation::core::UserAbortTriggeredEvent& bo)
    {
        aron::fromAron(dto.pose, bo.pose.matrix());
    }


    void
    toAron(armarx::navigation::core::arondto::SafetyThrottlingTriggeredEvent& dto,
           const armarx::navigation::core::SafetyThrottlingTriggeredEvent& bo)
    {
        aron::toAron(dto.pose, bo.pose.matrix());
        aron::toAron(dto.throttlingFactor, bo.throttlingFactor);
    }

    void
    fromAron(const armarx::navigation::core::arondto::SafetyThrottlingTriggeredEvent& dto,
             armarx::navigation::core::SafetyThrottlingTriggeredEvent& bo)
    {
        aron::fromAron(dto.pose, bo.pose.matrix());
        aron::fromAron(dto.throttlingFactor, bo.throttlingFactor);
    }

    void
    toAron(armarx::navigation::core::arondto::SafetyStopTriggeredEvent& dto,
           const armarx::navigation::core::SafetyStopTriggeredEvent& bo)
    {
        aron::toAron(dto.pose, bo.pose.matrix());
    }

    void
    fromAron(const armarx::navigation::core::arondto::SafetyStopTriggeredEvent& dto,
             armarx::navigation::core::SafetyStopTriggeredEvent& bo)
    {
        aron::fromAron(dto.pose, bo.pose.matrix());
    }

} // namespace armarx::navigation::core
