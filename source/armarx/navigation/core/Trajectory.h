/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <cstddef>
#include <memory>

#include <armarx/navigation/core/basic_types.h>

namespace armarx::navigation::core
{

    struct TrajectoryPoint
    {
        Waypoint waypoint;
        float velocity; // [mm/s]
    };

    using TrajectoryPoints = std::vector<TrajectoryPoint>;

    struct Projection
    {
        TrajectoryPoint projection;

        TrajectoryPoint wayPointBefore;
        TrajectoryPoint wayPointAfter;

        enum class Segment
        {
            FIRST,
            INTERMEDIATE,
            FINAL
        };

        Segment segment;
    };

    enum class VelocityInterpolation
    {
        LinearInterpolation,
        LastWaypoint
    };

    using TrajectoryPtr = std::shared_ptr<class Trajectory>;

    class Trajectory
    {
    public:
        Trajectory() = default;

        Trajectory(const std::vector<TrajectoryPoint>& points) : pts(points)
        {
        }

        Projection getProjection(const Position& point,
                                 const VelocityInterpolation& velocityInterpolation) const;

        [[nodiscard]] std::vector<Position> positions() const noexcept;

        [[nodiscard]] std::vector<Pose> poses() const noexcept;

        //! Note: the velocity will not be set!
        // currently, only 2D version
        // y is pointing forward
        static Trajectory FromPath(const Path& path, float velocity);

        //! Note: the velocity will not be set!
        // currently, only 2D version
        // y is pointing forward
        static Trajectory
        FromPath(const Pose& start, const Positions& waypoints, const Pose& goal, float velocity);

        Trajectory resample(float eps) const;

        float length() const;

        void setMaxVelocity(float maxVelocity);

        bool hasMaxDistanceBetweenWaypoints(float maxDistance) const;

        const std::vector<TrajectoryPoint>& points() const;

        std::vector<TrajectoryPoint>& mutablePoints();

        float duration(core::VelocityInterpolation interpolation) const;

        bool isValid() const noexcept;

        using Indices = std::vector<std::size_t>;

        /**
         * @brief get all points within a certain radius that are directly connected to idx 
         * 
         * @return the list without idx
         * 
         */
        Indices allConnectedPointsInRange(std::size_t idx, float radius) const;


    private:
        std::vector<TrajectoryPoint> pts;
    };

} // namespace armarx::navigation::core
