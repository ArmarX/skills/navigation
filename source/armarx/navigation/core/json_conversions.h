/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <SimoxUtility/json/json.hpp>

#include <RobotAPI/libraries/aron/core/codegeneration/cpp/AronGeneratedClass.h>
#include <RobotAPI/libraries/aron/core/data/rw/reader/nlohmannJSON/NlohmannJSONReader.h>
#include <RobotAPI/libraries/aron/core/data/rw/writer/nlohmannJSON/NlohmannJSONWriter.h>

namespace armarx
{
    template <typename AronDTO>
    void
    from_json(const nlohmann::json& j, AronDTO& dto)
    {
        static_assert(
            std::is_base_of<armarx::aron::codegenerator::cpp::AronGeneratedClass, AronDTO>::value);

        armarx::aron::data::reader::NlohmannJSONReader reader;
        dto.read(reader, j);
    }


    template <typename AronDTO>
    void
    to_json(nlohmann::json& j, const AronDTO& bo)
    {
        static_assert(
            std::is_base_of<armarx::aron::codegenerator::cpp::AronGeneratedClass, AronDTO>::value);

        armarx::aron::data::writer::NlohmannJSONWriter writer;
        j = bo.write(writer);
    }

} // namespace armarx
