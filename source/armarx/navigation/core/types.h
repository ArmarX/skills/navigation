/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <chrono>
#include <mutex>
#include <optional>

#include <Eigen/Geometry>

#include <SimoxUtility/meta/enum/EnumNames.hpp>
#include <VirtualRobot/VirtualRobot.h>

// #include <ArmarXCore/interface/serialization/Eigen/Eigen_fdi.h>

#include <armarx/navigation/core/DynamicScene.h>
#include <armarx/navigation/core/Graph.h>
#include <armarx/navigation/core/StaticScene.h>
#include <armarx/navigation/core/basic_types.h>

namespace armarx::navigation::core
{
    enum class NavigationFrame
    {
        Relative,
        Absolute
    };

    inline const simox::meta::EnumNames<NavigationFrame> NavigationFrameNames{
        {NavigationFrame::Relative, "Relative"},
        {NavigationFrame::Absolute, "Absolute"}};


    struct VelocityLimits
    {
        float linear;
        float angular;
    };


    struct Twist
    {
        LinearVelocity linear;
        AngularVelocity angular;

        Pose poseDiff(float dt) const;

        static Twist Zero();
    };


    class TimeServerInterface;

    struct SceneGraph
    {
        core::Graphs subgraphs;
    };

    struct Scene
    {
        DateTime timestamp = DateTime::Invalid();

        std::optional<core::StaticScene> staticScene = std::nullopt;
        std::optional<core::DynamicScene> dynamicScene = std::nullopt;

        VirtualRobot::RobotPtr robot;

        std::optional<core::SceneGraph> graph;
    };

    struct PIDParams
    {
        float Kp{1.0};
        float Ki{0.F};
        float Kd{0.F};
    };

    struct TwistLimits
    {
        float linear{std::numeric_limits<float>::max()};
        float angular{std::numeric_limits<float>::max()};
    };

} // namespace armarx::navigation::core
