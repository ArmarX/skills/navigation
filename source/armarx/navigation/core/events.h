#pragma once


#include <string>

#include <ArmarXCore/core/time/DateTime.h>

#include <armarx/navigation/core/types.h>

namespace armarx::navigation::core
{

    namespace event_names
    {
        inline const std::string GlobalPlanningFailed = "GlobalPlanningFailedEvent";
        inline const std::string MovementStarted = "MovementStartedEvent";
        inline const std::string GoalReached = "GoalReachedEvent";
        inline const std::string WaypointReached = "WaypointReachedEvent";
        inline const std::string SafetyThrottlingTriggered = "SafetyThrottlingTriggeredEvent";
        inline const std::string SafetyStopTriggered = "SafetyStopTriggeredEvent";
        inline const std::string UserAbortTriggered = "UserAbortTriggeredEvent";
        inline const std::string InternalError = "InternalErrorEventEvent";
    }  // namespace event_names

    struct Event
    {
        armarx::core::time::DateTime timestamp;
    };

    struct GlobalPlanningFailedEvent : public Event
    {
        std::string message;
    };

    struct MovementStartedEvent : Event
    {
        core::Pose startPose;
    };

    /**
     * @brief Event describing that the targeted goal was successfully reached.
     */
    struct GoalReachedEvent : public Event
    {
        core::Pose pose;
    };

    /**
     * @brief Event describing that a user-defined waypoint was successfully reached.
     */
    struct WaypointReachedEvent : public Event
    {
        core::Pose pose;
        int waypointId;
    };

    /**
     * @brief Event desciribing that a significant safety throttling factor was reached.
     *
     * Can be configured.
     */
    struct SafetyThrottlingTriggeredEvent : public Event
    {
        core::Pose pose;

        /**
         * @brief Ratio of throttled velocity divided by desired velocity.
         *
         * Value between 0 and 1 where 0 means full stop and 1 full desired velocity without any throttling.
         */
        float throttlingFactor = 1.0;
        // TODO: Direction where safety-critical obstacle is (or range or whatever...).
    };

    /**
     * @brief Event describing that for security reasons, the robot was stopped completely.
     */
    struct SafetyStopTriggeredEvent : public Event
    {
        core::Pose pose;
        // TODO: Direction where safety-critical obstacle is (or range or whatever...).
    };

    /**
     * @brief Event describing that the user aborted the current execution.
     */
    struct UserAbortTriggeredEvent : public Event
    {
        core::Pose pose;
    };

    /**
     * @brief Event describing the occurance of an internal unhandled error.
     */
    struct InternalErrorEvent : public Event
    {
        core::Pose pose;
        std::string message;
    };

} // namespace armarx::navigation::core
