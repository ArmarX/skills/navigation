
armarx_add_qt_plugin(qt_plugin_LocationGraphEditor
    WIDGET_CONTROLLER
        armarx::navigation::qt_plugins::location_graph_editor::WidgetController
    SOURCES
        WidgetController.cpp

        FunctionalEventFilter.cpp
        GuiGraph.cpp
        Visu.cpp

        widgets/default_colors.cpp
        widgets/utils.cpp
        widgets/ConnectDialog.cpp
        widgets/EdgeTableWidget.cpp
        widgets/NewEntityIdDialog.cpp
        widgets/RobotVisuWidget.cpp
        widgets/VertexDataWidget.cpp
        widgets/VertexTableWidget.cpp

        widgets/graph_scene/ControlWidget.cpp
        widgets/graph_scene/Scene.cpp
        widgets/graph_scene/Widget.cpp
    HEADERS
        WidgetController.h

        FunctionalEventFilter.h
        GuiGraph.h
        Visu.h

        widgets/default_colors.h
        widgets/utils.h
        widgets/ConnectDialog.h
        widgets/EdgeTableWidget.h
        widgets/NewEntityIdDialog.h
        widgets/RobotVisuWidget.h
        widgets/VertexDataWidget.h
        widgets/VertexTableWidget.h

        widgets/graph_scene.h
        widgets/graph_scene/ControlWidget.h
        widgets/graph_scene/Scene.h
        widgets/graph_scene/Widget.h

    UI_FILES
        LocationGraphEditorWidget.ui
    RESOURCE_FILES
       icons.qrc
    DEPENDENCIES
        # ArmarXGui
        SimpleConfigDialog

        # RobotAPI
        ArViz
        armem

        armarx_navigation::location
        armarx_navigation::graph
)
