/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::gui-plugins::WidgetController
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <memory>
#include <string>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem/client/forward_declarations.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>

#include "GuiGraph.h"
#include "Visu.h"
#include "widgets/graph_scene.h"
#include <armarx/navigation/core/Graph.h>
#include <armarx/navigation/core/aron/Location.aron.generated.h>
#include <armarx/navigation/gui-plugins/LocationGraphEditor/ui_LocationGraphEditorWidget.h>


class QDialog;


namespace armarx::viz
{
    class Client;
}
namespace armarx::navigation::qt_plugins::location_graph_editor::utils
{
    enum class EdgeDirection;
}
namespace armarx::navigation::qt_plugins::location_graph_editor
{
    class EdgeTableWidget;
    class RobotVisuWidget;
    class VertexDataWidget;
    class VertexTableWidget;


    /**
    \page armarx_navigation-GuiPlugins-LocationGraphEditor LocationGraphEditor
    \brief The LocationGraphEditor allows visualizing ...

    \image html LocationGraphEditor.png
    The user can

    API Documentation \ref WidgetController

    \see LocationGraphEditorGuiPlugin
    */


    /**
     * \class LocationGraphEditorGuiPlugin
     * \ingroup ArmarXGuiPlugins
     * \brief LocationGraphEditorGuiPlugin brief description
     *
     * Detailed description
     */

    /**
     * \class WidgetController
     * \brief WidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT WidgetController :
        public armarx::ArmarXComponentWidgetControllerTemplate<WidgetController>
    {
        Q_OBJECT

        using This = WidgetController;


    public:
        static QString GetWidgetName();
        static QIcon GetWidgetIcon();


        explicit WidgetController();
        virtual ~WidgetController() override;


        //// @see ArmarXWidgetController::loadSettings()
        void loadSettings(QSettings* settings) override;
        /// @see ArmarXWidgetController::saveSettings()
        void saveSettings(QSettings* settings) override;


        QPointer<QDialog> getConfigDialog(QWidget* parent = nullptr) override;
        void configured() override;


        void onInitComponent() override;
        void onConnectComponent() override;


        //because the above load/save functions are *obviously* for "Save/Load Gui Config." :/
        virtual void loadAutomaticSettings();
        virtual void saveAutomaticSettings();


    signals:

        void connected();

        void locationMemoryChanged();
        void graphMemoryChanged();
        void memoryChanged();

        void graphChanged();


    private slots:

        // Load
        void queryMemory();
        armem::client::QueryResult queryLocations();
        armem::client::QueryResult queryGraphs();

        void loadGraph();
        bool loadGraphDialog();

        // Commit
        void commit();
        armem::CommitResult commitLocations();
        armem::EntityUpdateResult commitGraph();


        // View & Tables
        void updateGraphList();
        void updateGraphView();
        void updateVertexView(GuiGraph::Vertex vertex);
        void updateEdgeView(GuiGraph::Edge edge);
        void updateArViz();


        // Selection & Highlighting
        void selectVertex(QTableWidgetItem* vertexItem);
        void selectVertex(GuiGraph::Vertex vertex);
        void updateVertexHighlighting();
        void updateEdgeHighlighting();


        // Graph modification triggered by user interaction
        void createVertexDialog();

        void addEdges(QList<QPair<QTableWidgetItem*, QTableWidgetItem*>> vertexItems);
        void removeEdges(QList<QTableWidgetItem*> edgeItems);
        void removeEdgesOfVertex(QList<QTableWidgetItem*> vertexItems,
                                 utils::EdgeDirection direction);

        void createGraphDialog();


    private:
        void setGraph(const armem::MemoryID& entityID, const core::Graph& nav);
        void setEmptyGraph(const armem::MemoryID& entityID);

        GuiGraph::Vertex addVertex(semrel::ShapeID vertexID, const VertexData& defaultAttribs);
        GuiGraph::Vertex addVertex(semrel::ShapeID vertexID,
                                   const armem::wm::EntityInstance& locationInstance);

        GuiGraph::Edge addEdge(GuiGraph::ConstVertex source,
                               GuiGraph::ConstVertex target,
                               const EdgeData& defaultAttribs);

        void removeVertex(GuiGraph::Vertex& vertex);
        void removeEdge(GuiGraph::Edge& edge);

        void clearGraph();
        void clearEdges();
        void clearVertices();


        QString getGraphDisplayName(const armem::MemoryID& entityID, bool changed = false) const;


    private:
        /// Widget Form
        Ui::LocationGraphEditorWidget widget;
        QPointer<SimpleConfigDialog> configDialog;


        struct Remote
        {
            std::string memoryNameSystemName = "MemoryNameSystem";
            armem::client::MemoryNameSystem memoryNameSystem;

            armem::client::Reader locationReader;
            armem::client::Writer locationWriter;

            armem::client::Reader graphReader;
            armem::client::Writer graphWriter;

            std::unique_ptr<viz::Client> arviz;

            void connect(Component& parent);
        };
        Remote remote;


        struct Model
        {
            armem::wm::Memory locationsMemory;

            armem::wm::Memory graphMemory;
            armem::MemoryID graphEntityID;
            GuiGraph graph;

            GraphVisu visu;
        };
        Model model;


        struct View
        {
            VertexTableWidget* vertexTable = nullptr;
            EdgeTableWidget* edgeTable = nullptr;
            VertexDataWidget* vertexData = nullptr;

            GraphSceneWidget* graph = nullptr;

            RobotVisuWidget* robotVisu = nullptr;
        };
        View view;


    private:
        QSettings settings;
        QString lastSelectedSceneName;
    };

} // namespace armarx::navigation::qt_plugins::location_graph_editor

namespace armarx
{
    using armarx::navigation::qt_plugins::location_graph_editor::WidgetController;
}
