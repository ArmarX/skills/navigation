/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <optional>

#include <SemanticObjectRelations/RelationGraph/RelationGraph.h>
#include <armarx/navigation/core/Graph.h>


class QGraphicsEllipseItem;
class QGraphicsLineItem;
class QTableWidgetItem;


namespace armarx::navigation::qt_plugins::location_graph_editor
{

    /**
     * @brief The NodeData struct holds data required for the node.
     * The name is stored in the key used in the map vertices.
     */
    struct VertexData : public navigation::core::VertexAttribs
    {
        /// The ellipse in the scene.
        QGraphicsEllipseItem* graphicsItem = nullptr;

        /// The item in the table tableWidgetVertices.
        QTableWidgetItem* tableWidgetItem = nullptr;

        /// Whether the node is highlighted.
        bool highlighted = false;

        /// Whether the vertex was changed since loading or committing.
        bool changed = false;
    };


    /**
     * @brief The EdgeData struct holds data required for the edge.
     * The name is stored in the key used in the map edges.
     */
    struct EdgeData : public navigation::core::EdgeAttribs
    {
        /// The line in the scene.
        QGraphicsLineItem* graphicsItem = nullptr;

        /// The item in the table tableWidgetEdges.
        QTableWidgetItem* tableWidgetItem = nullptr;

        /// Whether the edge is highlighted.
        bool highlighted = false;
    };


    struct GraphData : public navigation::core::GraphAttribs
    {
        /// Whether the graph structure was changed since loading or committing.
        bool edgesChanged = false;
    };


    class GuiGraph : public semrel::RelationGraph<VertexData, EdgeData, GraphData>
    {
    public:
        using RelationGraph::RelationGraph;


        bool hasChanged() const;

        std::optional<Vertex> getVertexFromTableItem(QTableWidgetItem* item);

        std::map<QTableWidgetItem*, Vertex> getTableItemToVertexMap();
        std::map<QTableWidgetItem*, Edge> getTableItemToEdgeMap();
    };


    GuiGraph toGuiGraph(const core::Graph& graph);
    core::Graph fromGuiGraph(const GuiGraph& graph);


    float getYawAngleDegree(const core::Pose& pose);
    double getYawAngleDegree(const core::Pose_d& pose);

} // namespace armarx::navigation::qt_plugins::location_graph_editor
