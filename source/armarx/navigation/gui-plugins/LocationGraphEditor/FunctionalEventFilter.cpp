#include "FunctionalEventFilter.h"


namespace simox::gui
{

    FunctionalEventFilter::FunctionalEventFilter(Function&& function) : function(function)
    {
    }


    bool
    FunctionalEventFilter::eventFilter(QObject* obj, QEvent* event)
    {
        return function(obj, event);
    }

} // namespace simox::gui
