/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "EdgeTableWidget.h"

#include <QAction>
#include <QHeaderView>
#include <QMenu>

#include "utils.h"


namespace armarx::navigation::qt_plugins::location_graph_editor
{

    EdgeTableWidget::EdgeTableWidget()
    {
        QStringList columns{"Source", "Target"};
        setColumnCount(columns.size());
        setHorizontalHeaderLabels(columns);
        horizontalHeader()->setResizeMode(0, QHeaderView::Stretch);
        horizontalHeader()->setResizeMode(1, QHeaderView::Stretch);
        horizontalHeader()->setVisible(true);

        setEditTriggers(QAbstractItemView::NoEditTriggers);
        setSortingEnabled(true);

        setAlternatingRowColors(true);

        QString styleSheet = this->styleSheet();
        styleSheet = styleSheet + "\n" + "selection-background-color: #FF8000;";
        setStyleSheet(styleSheet);

        setContextMenuPolicy(Qt::CustomContextMenu);
        connect(this, &This::customContextMenuRequested, this, &This::makeContextMenu);
    }


    QTableWidgetItem*
    EdgeTableWidget::addEdge(const core::VertexAttribs& sourceAttrib,
                             const core::VertexAttribs& targetAttrib)
    {
        QTableWidgetItem* result = nullptr;

        setSortingEnabled(false);
        {
            int row = rowCount();
            setRowCount(row + 1);

            setItem(row, 0, new QTableWidgetItem{QString::fromStdString(sourceAttrib.getName())});
            setItem(row, 1, new QTableWidgetItem{QString::fromStdString(targetAttrib.getName())});

            result = item(row, 0);
        }
        setSortingEnabled(true);

        return result;
    }


    void
    EdgeTableWidget::updateEdge(GuiGraph::Edge edge)
    {
        QColor bgColor = edge.attrib().highlighted ? bgColorSelected : bgColorDefault;
        QFont font;
        font.setBold(edge.attrib().highlighted);

        setSortingEnabled(false);
        {
            int row = this->row(edge.attrib().tableWidgetItem);
            for (int col = 0; col < 2; ++col)
            {
                auto* item = this->item(row, col);
                ARMARX_CHECK_NOT_NULL(item);

                item->setData(Qt::BackgroundRole, bgColor);
                item->setFont(font);
            }
        }
        setSortingEnabled(true);
    }


    void
    EdgeTableWidget::removeEdge(GuiGraph::Edge& edge)
    {
        if (currentItem() == edge.attrib().tableWidgetItem)
        {
            setCurrentItem(nullptr);
        }

        removeRow(row(edge.attrib().tableWidgetItem));
        edge.attrib().tableWidgetItem = nullptr;
    }


    QList<QTableWidgetItem*>
    EdgeTableWidget::selectedEdgeItems()
    {
        return utils::getSelectedItemsOfColumn(this, 0);
    }


    void
    EdgeTableWidget::makeContextMenu(QPoint pos)
    {
        QList<QTableWidgetItem*> items = selectedEdgeItems();

        QMenu menu;
        if (items.size() == 0)
        {
            QAction* action = menu.addAction("No edges selected");
            action->setEnabled(false);
        }
        else
        {
            QString desc;
            if (items.size() == 1)
            {
                desc = "edge '" + items[0]->text() + "' " + utils::arrowRight + " '" +
                       item(row(items[0]), 1)->text() + "'";
            }
            else
            {
                desc = QString::number(items.size()) + " edges";
            }

            menu.addSection("Selected " + desc);
            connect(menu.addAction("Remove " + desc),
                    &QAction::triggered,
                    [this, &items]() { emit edgeRemovalRequested(items); });
        }

        menu.exec(mapToGlobal(pos));
    }

} // namespace armarx::navigation::qt_plugins::location_graph_editor
