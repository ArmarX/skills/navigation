#include "Scene.h"

#include <Eigen/Core>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SemanticObjectRelations/Shapes/Shape.h>
#include <armarx/navigation/core/types.h>


namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene
{

    QGraphicsEllipseItem*
    Scene::addVertex(semrel::ShapeID vertexID, const core::VertexAttribs& attrib)
    {
        const core::Pose_d pose = attrib.getPose().cast<qreal>();

        GraphVisualizerGraphicsEllipseItem* item = new GraphVisualizerGraphicsEllipseItem{
            [this, vertexID]() { emit vertexSelected(vertexID); },
            pose.translation().x(),
            -pose.translation().y(),
            2,
            2};
        addItem(item);

        // setToolTip on graphicsItem does not work
        item->setZValue(std::numeric_limits<qreal>::max());
        // dynamic_cast<QGraphicsItem*>(item)->setToolTip(QString::fromStdString("Vertex '" + name + "'"));
        item->setToolTip(QString::fromStdString("Vertex '" + attrib.getName() + "'"));

        return item;
    }


    QGraphicsLineItem*
    Scene::addEdge(semrel::ShapeID sourceID,
                   const core::VertexAttribs& sourceAttrib,
                   semrel::ShapeID targetID,
                   const core::VertexAttribs& targetAttrib)
    {
        const core::Pose_d sourcePose = sourceAttrib.getPose().cast<qreal>();
        const core::Pose_d targetPose = targetAttrib.getPose().cast<qreal>();

        GraphVisualizerGraphicsLineItem* item = new GraphVisualizerGraphicsLineItem{
            [this, sourceID, targetID]() { emit edgeSelected(sourceID, targetID); },
            sourcePose(0, 3),
            -sourcePose(1, 3),
            targetPose(0, 3),
            -targetPose(1, 3)};
        addItem(item);

        // setToolTip on item does not work
        std::stringstream toolTip;
        toolTip << "Edge '" << sourceAttrib.getName() << "' -> '" << targetAttrib.getName();
        item->setToolTip(QString::fromStdString(toolTip.str()));

        return item;
    }


    void
    Scene::updateVertex(GuiGraph::Vertex& vertex)
    {
        QGraphicsEllipseItem* item = vertex.attrib().graphicsItem;
        ARMARX_CHECK_NOT_NULL(item);

        const core::Pose_d pose = vertex.attrib().getPose().cast<qreal>();

        QColor color = vertex.attrib().highlighted ? colorSelected : colorDefault;
        double lineWidth = vertex.attrib().highlighted ? lineWidthSelected : lineWidthDefault;

        item->setPen(QPen{color});
        item->setBrush(QBrush{color});
        item->setRect(pose.translation().x() - lineWidth * verticesScaleFactor / 2,
                      -pose.translation().y() - lineWidth * verticesScaleFactor / 2,
                      lineWidth * verticesScaleFactor,
                      lineWidth * verticesScaleFactor);
    }


    void
    Scene::updateEdge(GuiGraph::Edge& edge)
    {
        QGraphicsLineItem* item = edge.attrib().graphicsItem;
        ARMARX_CHECK_NOT_NULL(item)
            << edge.source().attrib().getName() << " -> " << edge.target().attrib().getName();

        const core::Pose_d sourcePose = edge.source().attrib().getPose().cast<qreal>();
        const core::Pose_d targetPose = edge.target().attrib().getPose().cast<qreal>();

        QColor color = edge.attrib().highlighted ? colorSelected : colorDefault;
        double lineWidth = edge.attrib().highlighted ? lineWidthSelected : lineWidthDefault;

        QPen pen{color};
        pen.setWidthF(lineWidth * lineScaleFactor);

        item->setPen(pen);
        item->setLine(sourcePose(0, 3), -sourcePose(1, 3), targetPose(0, 3), -targetPose(1, 3));
    }


    void
    Scene::removeVertex(QGraphicsEllipseItem*& item)
    {
        removeItem(item);
        delete item;
        item = nullptr;
    }


    void
    Scene::removeEdge(QGraphicsLineItem*& item)
    {
        removeItem(item);
        delete item;
        item = nullptr;
    }


    GraphVisualizerGraphicsEllipseItem::GraphVisualizerGraphicsEllipseItem(
        std::function<void(void)> onDoubleClicked,
        qreal x,
        qreal y,
        qreal width,
        qreal height,
        QGraphicsItem* parent) :
        QGraphicsEllipseItem{x, y, width, height, parent}, onDoubleClicked{onDoubleClicked}
    {
    }


    GraphVisualizerGraphicsEllipseItem::~GraphVisualizerGraphicsEllipseItem()
    {
    }


    void
    GraphVisualizerGraphicsEllipseItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent*)
    {
        onDoubleClicked();
    }


    GraphVisualizerGraphicsLineItem::GraphVisualizerGraphicsLineItem(
        std::function<void(void)> onDoubleClicked,
        qreal x1,
        qreal y1,
        qreal x2,
        qreal y2,
        QGraphicsItem* parent) :
        QGraphicsLineItem{x1, y1, x2, y2, parent}, onDoubleClicked{onDoubleClicked}
    {
    }


    GraphVisualizerGraphicsLineItem::~GraphVisualizerGraphicsLineItem()
    {
    }


    void
    GraphVisualizerGraphicsLineItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent*)
    {
        onDoubleClicked();
    }

} // namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene
