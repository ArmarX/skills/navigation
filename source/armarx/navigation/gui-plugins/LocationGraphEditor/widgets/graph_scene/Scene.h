#pragma once

#include <QGraphicsEllipseItem>
#include <QGraphicsLineItem>
#include <QGraphicsScene>
#include <functional>

#include <armarx/navigation/core/Graph.h>
#include <armarx/navigation/gui-plugins/LocationGraphEditor/GuiGraph.h>


namespace semrel
{
    struct ShapeID;
}
namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene
{
    class Scene : public QGraphicsScene
    {
        Q_OBJECT

    public:
        using QGraphicsScene::QGraphicsScene;

        template <class VertexT>
        QGraphicsEllipseItem*
        addVertex(const VertexT& vertex)
        {
            return addVertex(vertex.objectID(), vertex.attrib());
        }
        QGraphicsEllipseItem* addVertex(semrel::ShapeID vertexID,
                                        const core::VertexAttribs& attribs);


        template <class EdgeT>
        QGraphicsLineItem*
        addEdge(const EdgeT& edge)
        {
            return addEdge(edge.sourceObjectID(),
                           edge.source().attrib(),
                           edge.targetObjectID(),
                           edge.target().attrib());
        }
        QGraphicsLineItem* addEdge(semrel::ShapeID sourceID,
                                   const core::VertexAttribs& sourceAttrib,
                                   semrel::ShapeID targetID,
                                   const core::VertexAttribs& targetAttrib);


        void updateVertex(GuiGraph::Vertex& vertex);
        void updateEdge(GuiGraph::Edge& edge);

        void removeVertex(QGraphicsEllipseItem*& item);
        void removeEdge(QGraphicsLineItem*& item);


    public slots:


    signals:

        void vertexSelected(const semrel::ShapeID& vertexID);
        void edgeSelected(const semrel::ShapeID& sourceID, const semrel::ShapeID& targetID);


    public:
        double lineWidthDefault = 5;
        double lineWidthSelected = 10;

        double sceneScaleFactor = 2;
        double verticesScaleFactor = 3.0 * sceneScaleFactor;
        double lineScaleFactor = 1.0 * sceneScaleFactor;

        QColor colorDefault = QColor::fromRgb(128, 128, 255);
        QColor colorSelected = QColor::fromRgb(255, 128, 0);
    };


    /**
     * @brief Required to override the double click event.
     * This is required to toggle the select state.
     */
    class GraphVisualizerGraphicsEllipseItem : public QGraphicsEllipseItem
    {
    public:
        GraphVisualizerGraphicsEllipseItem(std::function<void(void)> onDoubleClicked,
                                           qreal x,
                                           qreal y,
                                           qreal width,
                                           qreal height,
                                           QGraphicsItem* parent = nullptr);

        ~GraphVisualizerGraphicsEllipseItem() override;


    protected:
        void mouseDoubleClickEvent(QGraphicsSceneMouseEvent*) override;


        std::function<void(void)> onDoubleClicked;
    };


    /**
     * @brief Required to override the double click event.
     * This is required to toggle the select state.
     */
    class GraphVisualizerGraphicsLineItem : public QGraphicsLineItem
    {
    public:
        GraphVisualizerGraphicsLineItem(std::function<void(void)> onDoubleClicked,
                                        qreal x1,
                                        qreal y1,
                                        qreal x2,
                                        qreal y2,
                                        QGraphicsItem* parent = nullptr);

        ~GraphVisualizerGraphicsLineItem() override;


    protected:
        void mouseDoubleClickEvent(QGraphicsSceneMouseEvent*) override;


        std::function<void(void)> onDoubleClicked;
    };


} // namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene
