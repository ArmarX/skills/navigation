#pragma once

#include <QWidget>

class QDoubleSpinBox;
class QHBoxLayout;
class QPushButton;


namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene
{

    class ControlWidget : public QWidget
    {
        Q_OBJECT
        using This = ControlWidget;


    public:
        ControlWidget();

        qreal angle();
        void setAngle(qreal angle);
        qreal zoom();
        void setZoom(qreal zoom);


    signals:

        void angleChanged(qreal value);

        void zoomChanged(qreal value);
        void zoomAutoAdjustRequested();


    public:
        struct Angle
        {
            QPushButton* turnClockwise = nullptr;
            QDoubleSpinBox* value = nullptr;
            QPushButton* turnCounterClockwise = nullptr;

            qreal rotateStepSize = 45;
        };
        Angle _angle;

        struct Zoom
        {
            QDoubleSpinBox* value = nullptr;
            QPushButton* audoAdjust = nullptr;
        };
        Zoom _zoom;
    };

} // namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene
