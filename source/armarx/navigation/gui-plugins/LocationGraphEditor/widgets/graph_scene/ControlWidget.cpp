#include "ControlWidget.h"

// #include <ArmarXCore/core/logging/Logging.h>

#include <QDoubleSpinBox>
#include <QFrame>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <cmath>


namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene
{

    ControlWidget::ControlWidget()
    {
        _angle.turnClockwise = new QPushButton("\u21bb"); // https://unicode-table.com/de/21BB/
        _angle.turnCounterClockwise =
            new QPushButton("\u21ba"); // https://unicode-table.com/de/21BA/
        std::vector<QPushButton*> turnButtons;
        for (QPushButton* button : turnButtons)
        {
            button->setMinimumWidth(button->minimumHeight());
            button->setMaximumWidth(button->maximumHeight());
            button->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        }

        _angle.value = new QDoubleSpinBox();
        _angle.value->setMinimum(-360);
        _angle.value->setMaximum(360);
        _angle.value->setSingleStep(15);
        _angle.value->setValue(0);

        _zoom.value = new QDoubleSpinBox();
        _zoom.value->setDecimals(5);
        _zoom.value->setMinimum(1e-6);
        _zoom.value->setSingleStep(0.01);
        _zoom.value->setValue(0.1);
        _zoom.audoAdjust = new QPushButton("Auto");


        QHBoxLayout* layout = new QHBoxLayout();
        this->setLayout(layout);

        layout->addWidget(new QLabel("Angle: "));
        layout->addWidget(_angle.turnClockwise);
        layout->addWidget(_angle.value);
        layout->addWidget(_angle.turnCounterClockwise);

        // Vertical separator line.
        QFrame* line = new QFrame();
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        layout->addWidget(line);

        layout->addWidget(new QLabel("Zoom: "));
        layout->addWidget(_zoom.value);
        layout->addWidget(_zoom.audoAdjust);


        // Connect

        connect(_angle.value,
                QOverload<double>::of(&QDoubleSpinBox::valueChanged),
                this,
                &This::angleChanged);

        connect(_angle.turnClockwise,
                &QPushButton::pressed,
                [this]()
                {
                    qreal newangle = std::fmod(angle() + _angle.rotateStepSize, 360.0);
                    setAngle(newangle);
                });
        connect(_angle.turnCounterClockwise,
                &QPushButton::pressed,
                [this]() { setAngle(std::fmod(angle() - _angle.rotateStepSize, 360.0)); });

        connect(_zoom.value,
                QOverload<double>::of(&QDoubleSpinBox::valueChanged),
                this,
                &This::angleChanged);
        connect(_zoom.audoAdjust, &QPushButton::pressed, this, &This::zoomAutoAdjustRequested);
    }


    qreal
    ControlWidget::angle()
    {
        return _angle.value->value();
    }

    void
    ControlWidget::setAngle(qreal angle)
    {
        _angle.value->setValue(angle);
    }


    qreal
    ControlWidget::zoom()
    {
        return _zoom.value->value();
    }

    void
    ControlWidget::setZoom(qreal zoom)
    {
        _zoom.value->setValue(zoom);
    }

} // namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene
