#include "Widget.h"

#include <QGraphicsView>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>

#include "ControlWidget.h"
#include "Scene.h"


namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene
{

    Widget::Widget()
    {
        _control = new ControlWidget();

        _scene = new Scene();

        _view = new QGraphicsView();
        _view->setScene(_scene);
        _view->setRenderHint(QPainter::Antialiasing, true);

        int margin = 0;
        this->setContentsMargins(margin, margin, margin, margin);
        this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

        QHBoxLayout* header = new QHBoxLayout();
        header->setSpacing(0);
        header->addWidget(new QLabel("Graph"));
        header->addStretch();
        header->addWidget(_control);

        QVBoxLayout* layout = new QVBoxLayout();
        this->setLayout(layout);

        layout->addLayout(header);
        layout->addWidget(_view);


        connect(_control, &ControlWidget::angleChanged, this, &This::transform);
        connect(_control, &ControlWidget::zoomChanged, this, &This::transform);
        connect(_control, &ControlWidget::zoomAutoAdjustRequested, this, &This::autoAdjustZoom);


        // Initial transform.
        transform();

#if 0
        auto eventFilter = [this](QObject* obj, QEvent* event) -> bool
        {
            if (obj == this->view.view && event->type() == QEvent::MouseButtonPress)
            {
                QMouseEvent* me = static_cast<QMouseEvent*>(event);
                if (me->button() == Qt::LeftButton)
                {
                    QPointF scenePoint = this->view.view->mapToScene(me->pos());
                    scenePoint.setY(- scenePoint.y());  // not sure why

                    float minDist = std::numeric_limits<float>::max();
                    auto bestIt = this->vertices.cend();

                    for (auto it = this->vertices.cbegin(); it != this->vertices.cend(); ++it)
                    {
                        float deltaX = it->second.pose->position->x - scenePoint.x();
                        float deltaY = it->second.pose->position->y - scenePoint.y();
                        float dist = std::sqrt(deltaX * deltaX + deltaY * deltaY);

                        if (dist < minDist)
                        {
                            minDist = dist;
                            bestIt = it;
                        }
                    }

                    if (bestIt != this->vertices.cend())
                    {
                        this->highlightVertex(bestIt->first);
                    }
                }
            }
            else if (event->type() == QEvent::Resize)
            {
                this->adjustView();
            }
            else
            {
                return false;
            }
        };
        _view->installEventFilter(new simox::gui::FunctionalEventFilter(eventFilter));
#endif
    }


    QGraphicsView*
    Widget::view()
    {
        return _view;
    }


    Scene*
    Widget::scene()
    {
        return _scene;
    }


    void
    Widget::transform()
    {
        double angle = _control->angle();
        double zoom = _control->zoom();
        _view->setTransform(QTransform::fromScale(zoom, zoom).rotate(angle));
    }


    void
    Widget::autoAdjustZoom()
    {
        int margin = 3;
        QRectF rect = _scene->sceneRect() + QMargins(margin, margin, margin, margin);
        _view->fitInView(rect, Qt::AspectRatioMode::KeepAspectRatio);
        _control->setZoom(_view->transform().rotate(-_control->angle()).m11());
    }

} // namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene
