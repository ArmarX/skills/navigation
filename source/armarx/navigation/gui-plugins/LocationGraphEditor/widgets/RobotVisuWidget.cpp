/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotVisuWidget.h"

#include <QCheckBox>
#include <QDialog>
#include <QDialogButtonBox>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/components/ArViz/Client/Elements.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include "ConnectDialog.h"
#include <QtWidgets/QLabel>


namespace armarx::navigation::qt_plugins::location_graph_editor
{

    robotvisu::SettingsWidget::SettingsWidget(QWidget* parent) : QWidget(parent)
    {
        _enabled = new QCheckBox("Enable Visu", this);
        _enabled->setChecked(true);

        _collisionModel = new QCheckBox("Use Collision Model", this);
        _collisionModel->setChecked(false);


        setLayout(new QVBoxLayout());
        layout()->addWidget(_enabled);
        layout()->addWidget(_collisionModel);


        connect(_enabled, &QCheckBox::toggled, this, &This::changed);
        connect(_collisionModel, &QCheckBox::toggled, this, &This::changed);

        const int margin = 0;
        setContentsMargins(margin, margin, margin, margin);
    }


    bool
    robotvisu::SettingsWidget::isEnabled() const
    {
        return _enabled->isChecked();
    }


    bool
    robotvisu::SettingsWidget::useCollisionModel() const
    {
        return _collisionModel->isChecked();
    }


    robotvisu::Connection::Connection(RobotStateComponentInterfacePrx robotStateComponent,
                                      robotvisu::SettingsWidget* settings) :
        _robotStateComponent(
            std::make_unique<RobotStateComponentInterfacePrx>(robotStateComponent)),
        _settings(settings)
    {
        ARMARX_CHECK_NOT_NULL(robotStateComponent);
        _filename = robotStateComponent->getRobotFilename();
        _robot = RemoteRobot::createLocalClone(
            robotStateComponent, "", {}, VirtualRobot::RobotIO::RobotDescription::eStructure);
        ARMARX_CHECK_NOT_NULL(_robot);
    }


    robotvisu::Connection::~Connection()
    {
    }


    core::Pose
    robotvisu::Connection::getGlobalPose(bool synchronize)
    {
        _synchronize(synchronize);
        return core::Pose{_robot->getGlobalPose()};
    }


    std::map<std::string, float>
    robotvisu::Connection::getJointValues(bool synchronize)
    {
        _synchronize(synchronize);
        return _robot->getConfig()->getRobotNodeJointValueMap();
    }


    viz::Robot
    robotvisu::Connection::vizRobot(const std::string& id, bool synchronize)
    {
        _synchronize(synchronize);
        viz::Robot robot = viz::Robot(id)
                               .file("", _filename)
                               .pose(getGlobalPose(false))
                               .joints(getJointValues(false));

        if (_settings and _settings->useCollisionModel())
        {
            robot.useCollisionModel();
        }
        return robot;
    }


    RobotStateComponentInterfacePrx
    robotvisu::Connection::getRobotStateComponent() const
    {
        ARMARX_CHECK_NOT_NULL(_robotStateComponent);
        return *_robotStateComponent;
    }


    std::string
    robotvisu::Connection::getConnectedName() const
    {
        return getRobotStateComponent()->ice_getIdentity().name;
    }


    void
    robotvisu::Connection::_synchronize(bool synchronize)
    {
        if (synchronize)
        {
            RemoteRobot::synchronizeLocalClone(_robot, getRobotStateComponent());
        }
    }


    RobotVisuWidget::RobotVisuWidget(ManagedIceObject& component, QWidget* parent) :
        QWidget(parent), _component(component)
    {
        _statusLabel = new QLabel("Not connected", this);
        _connectButton = new QPushButton("Connect ...", this);

        _settings = new robotvisu::SettingsWidget();
        _settings->setEnabled(false);


        QBoxLayout* layout = new QVBoxLayout(this);
        setLayout(layout);

        layout->addWidget(_statusLabel);
        layout->addWidget(_connectButton);
        layout->addWidget(_settings);


        connect(_connectButton, &QPushButton::pressed, this, &This::connectToRobot);

        connect(_settings, &robotvisu::SettingsWidget::changed, this, &This::settingsChanged);

        connect(this,
                &This::connected,
                this,
                [this]()
                {
                    QString name = QString::fromStdString(connection().getConnectedName());
                    _statusLabel->setText("Connected to '" + name + "'");
                });
        connect(this, &This::connected, this, [this]() { _settings->setEnabled(true); });
    }


    bool
    RobotVisuWidget::isEnabled() const
    {
        return isConnected() and _settings->isEnabled();
    }


    bool
    RobotVisuWidget::isConnected() const
    {
        return _connection.has_value();
    }


    robotvisu::Connection&
    RobotVisuWidget::connection()
    {
        return _connection.value();
    }


    void
    RobotVisuWidget::connectToRobot()
    {
        ConnectDialog<RobotStateComponentInterfacePrx> dialog(
            "*RobotStateComponent", _component, this);
        switch (dialog.exec())
        {
            case QDialog::Accepted:
                break;
            case QDialog::Rejected:
                return;
        }

        auto robotStateComponent = dialog.getProxy();
        if (robotStateComponent)
        {
            _connection.emplace(robotStateComponent, _settings);
            emit connected();
        }
    }

} // namespace armarx::navigation::qt_plugins::location_graph_editor
