/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QColor>
#include <QList>
#include <QTableWidget>

#include <armarx/navigation/gui-plugins/LocationGraphEditor/GuiGraph.h>
#include <armarx/navigation/gui-plugins/LocationGraphEditor/widgets/default_colors.h>


namespace armarx::navigation::qt_plugins::location_graph_editor
{

    class EdgeTableWidget : public QTableWidget
    {
        Q_OBJECT
        using This = EdgeTableWidget;


    public:
        EdgeTableWidget();


        template <class EdgeT>
        QTableWidgetItem*
        addEdge(const EdgeT& edge)
        {
            return addEdge(edge.source().attrib(), edge.target().attrib());
        }

        QTableWidgetItem* addEdge(const core::VertexAttribs& sourceAttrib,
                                  const core::VertexAttribs& targetAttrib);


        void updateEdge(GuiGraph::Edge edge);

        void removeEdge(GuiGraph::Edge& edge);

        void clearEdges();


        QList<QTableWidgetItem*> selectedEdgeItems();


    signals:

        void edgeRemovalRequested(QList<QTableWidgetItem*> edgeItems);


    public slots:

        void makeContextMenu(QPoint pos);


    public:
        QColor bgColorDefault = default_colors::tableBackgroundDefault;
        QColor bgColorSelected = default_colors::tableBackgroundSelected;
    };

} // namespace armarx::navigation::qt_plugins::location_graph_editor
