/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VertexTableWidget.h"

#include <QAction>
#include <QHeaderView>
#include <QMenu>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include "utils.h"


namespace armarx::navigation::qt_plugins::location_graph_editor
{

    VertexTableWidget::VertexTableWidget()
    {
        QStringList columns{"Name", "X [mm]", "Y [mm]", "Yaw [\u00b0]"};
        setColumnCount(columns.size());
        setHorizontalHeaderLabels(columns);
        horizontalHeader()->setResizeMode(0, QHeaderView::Stretch);
        horizontalHeader()->setVisible(true);

        setEditTriggers(QAbstractItemView::NoEditTriggers);
        setSortingEnabled(true);

        setAlternatingRowColors(true);

        QString styleSheet = this->styleSheet();
        styleSheet = styleSheet + "\n" + "selection-background-color: #FF8000;";
        setStyleSheet(styleSheet);

        setContextMenuPolicy(Qt::CustomContextMenu);
        connect(this, &This::customContextMenuRequested, this, &This::makeContextMenu);
    }


    QTableWidgetItem*
    VertexTableWidget::addVertex()
    {
        // We need to disable sorting to prevent the new row from being moved away.
        setSortingEnabled(false);

        QTableWidgetItem* result = nullptr;
        {
            int row = rowCount();
            setRowCount(row + 1);

            for (int col = 0; col < 4; ++col)
            {
                // Just fill with vanilla items, they will get values in the update.
                QTableWidgetItem* item = new QTableWidgetItem{};
                setItem(row, col, item);

                if (col >= 1)
                {
                    item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
                }
                if (col == 0)
                {
                    result = item;
                }
            }
        }

        // Enable sorting - `row` could now be moved away.
        setSortingEnabled(true);

        ARMARX_CHECK_NOT_NULL(result);
        return result;
    }


    void
    VertexTableWidget::updateVertex(GuiGraph::Vertex vertex)
    {
        const core::Pose_d pose = vertex.attrib().getPose().cast<qreal>();
        char format = 'f';
        const int precision = 2;

        // Changing the values may trigger a re-sort.
        setSortingEnabled(false);


        int row = this->row(vertex.attrib().tableWidgetItem);

        QString displayName = QString::fromStdString(vertex.attrib().getName());
        if (vertex.attrib().changed)
        {
            displayName += "*";
        }
        item(row, 0)->setText(displayName);
        item(row, 0)->setData(Qt::UserRole, QString::fromStdString(vertex.attrib().getName()));
        item(row, 1)->setText(QString::number(pose(0, 3), format, precision));
        item(row, 2)->setText(QString::number(pose(1, 3), format, precision));
        item(row, 3)->setText(QString::number(getYawAngleDegree(pose), format, precision));

        setSortingEnabled(true);

        QColor bgColor = vertex.attrib().highlighted ? bgColorSelected : bgColorDefault;
        QFont font;
        font.setBold(vertex.attrib().highlighted);
        for (int col = 0; col < 4; ++col)
        {
            auto* item = this->item(row, col);
            ARMARX_CHECK_NOT_NULL(item);

            item->setData(Qt::BackgroundRole, bgColor);
            item->setFont(font);
        }
    }


    void
    VertexTableWidget::removeVertex(GuiGraph::Vertex& vertex)
    {
        if (currentItem() == vertex.attrib().tableWidgetItem)
        {
            setCurrentItem(nullptr);
        }

        removeRow(row(vertex.attrib().tableWidgetItem));
        vertex.attrib().tableWidgetItem = nullptr;
    }


    QList<QTableWidgetItem*>
    VertexTableWidget::selectedVertexItems()
    {
        return utils::getSelectedItemsOfColumn(this, 0);
    }


    QString
    VertexTableWidget::_nameOf(QTableWidgetItem* item)
    {
        return item->data(Qt::UserRole).toString();
    }


    void
    VertexTableWidget::makeContextMenu(QPoint pos)
    {
        QList<QTableWidgetItem*> items = selectedVertexItems();

        QMenu menu;
        if (items.size() == 0)
        {
            QAction* action = menu.addSection("No locations selected");
            action->setEnabled(false);
        }

        // Partners selected
        if (items.size() == 2)
        {
            menu.addSection("Selected pair of locations");

            // Generate actions for connecting these two nodes.
            std::sort(items.begin(),
                      items.end(),
                      [](QTableWidgetItem* first, QTableWidgetItem* second)
                      { return _nameOf(first) < _nameOf(second); });
            QTableWidgetItem* first = items[0];
            QTableWidgetItem* second = items[1];

            connect(menu.addAction("Add edge '" + _nameOf(first) + "' " + utils::arrowRight + " '" +
                                   _nameOf(second) + "'"),
                    &QAction::triggered,
                    [this, first, second]() {
                        emit newEdgesRequested({{first, second}});
                    });
            connect(menu.addAction("Add edge '" + _nameOf(first) + "' " + utils::arrowLeft + " '" +
                                   _nameOf(second) + "'"),
                    &QAction::triggered,
                    [this, first, second]() {
                        emit newEdgesRequested({{second, first}});
                    });
            connect(menu.addAction("Add edges '" + _nameOf(first) + "' " + utils::arrowBoth + " '" +
                                   _nameOf(second) + "'"),
                    &QAction::triggered,
                    [this, first, second]() {
                        emit newEdgesRequested({{first, second}, {second, first}});
                    });
        }

        // Partners via context menu
        if (items.size() > 0)
        {
            QString edges = items.size() == 1 ? "edge" : "edges";
            QString desc = items.size() == 1 ? "'" + _nameOf(items[0]) + "'"
                                             : QString::number(items.size()) + " locations";

            if (items.size() == 1)
            {
                // QAction* deleteAction = menu.addAction("Delete location '" + );
                menu.addSection("Selected single location " + desc);
            }
            else
            {
                menu.addSection("Selected bulk of " + desc);
            }

            using Item = QTableWidgetItem;
            using ListOfEdges = QList<QPair<Item*, Item*>>;
            using AppendFunc =
                std::function<void(ListOfEdges & edges, Item * selected, Item * action)>;

            auto addBulkAddEdgeActions = [this, &items](QMenu* submenu, AppendFunc appendFunc)
            {
                if (items.size() == rowCount())
                {
                    QAction* a = submenu->addAction("No other locations");
                    a->setDisabled(true);
                }
                for (int row = 0; row < rowCount(); ++row)
                {
                    QTableWidgetItem* action = this->item(row, 0);
                    if (items.count(action) == 0) // Do no generate self-edges
                    {
                        QAction* a = submenu->addAction(_nameOf(action));
                        connect(a,
                                &QAction::triggered,
                                this,
                                [this, items, action, appendFunc]()
                                {
                                    QList<QPair<QTableWidgetItem*, QTableWidgetItem*>> edges;
                                    for (auto* selected : items)
                                    {
                                        appendFunc(edges, selected, action);
                                    }
                                    emit newEdgesRequested(edges);
                                });
                    }
                }
            };

            addBulkAddEdgeActions(
                menu.addMenu("Add " + edges + " " + desc + " " + utils::arrowRight + " ..."),
                [](ListOfEdges& edges, Item* selected, Item* action) {
                    edges.append(QPair{selected, action});
                });
            addBulkAddEdgeActions(
                menu.addMenu("Add " + edges + " " + desc + " " + utils::arrowLeft + " ..."),
                [](ListOfEdges& edges, Item* selected, Item* action) {
                    edges.append(QPair{action, selected});
                });
            addBulkAddEdgeActions(
                menu.addMenu("Add " + edges + " " + desc + " " + utils::arrowBoth + " ..."),
                [](ListOfEdges& edges, Item* selected, Item* action)
                {
                    edges.append(QPair{selected, action});
                    edges.append(QPair{action, selected});
                });


            auto connectBulkRemoveEdgeAction =
                [this, &items](QAction* action, utils::EdgeDirection edgeDirection)
            {
                connect(action,
                        &QAction::triggered,
                        this,
                        [this, items, edgeDirection]()
                        { emit edgeRemovalRequested(items, edgeDirection); });
            };

            connectBulkRemoveEdgeAction(
                menu.addAction("Remove all edges " + utils::arrowRight + " " + desc),
                utils::EdgeDirection::To);
            connectBulkRemoveEdgeAction(
                menu.addAction("Remove all edges " + utils::arrowLeft + " " + desc),
                utils::EdgeDirection::From);
            connectBulkRemoveEdgeAction(
                menu.addAction("Remove all edges " + utils::arrowBoth + " " + desc),
                utils::EdgeDirection::Bidirectional);
        }


        menu.addSection("Manage Locations");
        connect(menu.addAction("Create new location ..."),
                &QAction::triggered,
                this,
                [this]() { emit newVertexRequested(); });

        menu.exec(mapToGlobal(pos));
    }

} // namespace armarx::navigation::qt_plugins::location_graph_editor
