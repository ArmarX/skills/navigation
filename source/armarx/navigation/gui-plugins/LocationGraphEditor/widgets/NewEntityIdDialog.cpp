/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "NewEntityIdDialog.h"

#include <QDialogButtonBox>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/armem/core/MemoryID.h>


namespace armarx::navigation::qt_plugins::location_graph_editor
{

    NewEntityIdDialog::NewEntityIdDialog(const armem::MemoryID& coreSegmentID, QWidget* parent) :
        QDialog(parent), coreSegmentID(std::make_unique<armem::MemoryID>(coreSegmentID))
    {
        _providerSegmentName = new QLineEdit(this);
        _entityName = new QLineEdit(this);

        _buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);

        QLabel* instruction =
            new QLabel("Enter provider segment name and entity name for new entity:");
        QFont font = instruction->font();
        font.setBold(true);
        instruction->setFont(font);


        QGridLayout* grid = new QGridLayout();
        int col = 0;

        grid->addWidget(new QLabel("Core Segment ID"), 0, col);
        grid->addWidget(new QLabel(QString::fromStdString(coreSegmentID.str()) + "/"), 1, col);
        ++col;

        grid->addWidget(new QLabel("Provider Segment Name"), 0, col);
        grid->addWidget(_providerSegmentName, 1, col);
        ++col;

        grid->addWidget(new QLabel("/"), 1, col);
        ++col;

        grid->addWidget(new QLabel("Entity Name"), 0, col);
        grid->addWidget(_entityName, 1, col);
        ++col;


        QVBoxLayout* layout = new QVBoxLayout();
        setLayout(layout);
        layout->addWidget(instruction);
        layout->addLayout(grid);
        layout->addWidget(_buttonBox);


        connect(_buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
        connect(_buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

        auto enableOkIfReady = [this]()
        {
            _buttonBox->button(QDialogButtonBox::Ok)
                ->setEnabled(_providerSegmentName->text().size() > 0 and
                             _entityName->text().size() > 0);
        };
        connect(_providerSegmentName, &QLineEdit::textChanged, this, enableOkIfReady);
        connect(_entityName, &QLineEdit::textChanged, this, enableOkIfReady);

        enableOkIfReady();
    }


    NewEntityIdDialog::~NewEntityIdDialog()
    {
    }


    QString
    NewEntityIdDialog::providerSegmentName() const
    {
        return _providerSegmentName->text();
    }


    QString
    NewEntityIdDialog::entityName() const
    {
        return _entityName->text();
    }


    armem::MemoryID
    NewEntityIdDialog::entityIDWithoutCoreSegmentID() const
    {
        armem::MemoryID id;
        id.providerSegmentName = providerSegmentName().toStdString();
        id.entityName = entityName().toStdString();
        return id;
    }

    armem::MemoryID
    NewEntityIdDialog::entityID() const
    {
        armem::MemoryID entityID = entityIDWithoutCoreSegmentID();
        ARMARX_CHECK_NOT_NULL(coreSegmentID);
        entityID.setCoreSegmentID(*coreSegmentID);
        return entityID;
    }

} // namespace armarx::navigation::qt_plugins::location_graph_editor
