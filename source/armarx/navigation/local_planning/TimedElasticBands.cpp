#include "TimedElasticBands.h"

#include <armarx/navigation/local_planning/LocalPlanner.h>
#include <armarx/navigation/local_planning/core.h>

namespace armarx::navigation::loc_plan
{

    // TimedElasticBandsParams

    Algorithms
    TimedElasticBandsParams::algorithm() const
    {
        return Algorithms::TimedElasticBands;
    }

    aron::data::DictPtr
    TimedElasticBandsParams::toAron() const
    {
        return nullptr; // TODO implement
    }

    TimedElasticBandsParams
    TimedElasticBandsParams::FromAron(const aron::data::DictPtr& dict)
    {
        return TimedElasticBandsParams(); // TODO implement
    }

    // TimedElasticBands

    TimedElasticBands::TimedElasticBands(const Params& params, const core::Scene& ctx) :
        LocalPlanner(ctx), params(params)
    {
    }

    std::optional<LocalPlannerResult>
    TimedElasticBands::plan(const core::Trajectory& goal)
    {
        // TODO implement
        return {};
    }
} // namespace armarx::navigation::loc_plan
