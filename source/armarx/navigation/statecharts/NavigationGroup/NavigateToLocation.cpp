/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    navigation::NavigationGroup
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "NavigateToLocation.h"

#include <ArmarXCore/core/ManagedIceObject.h>

#include <RobotAPI/components/armem/MemoryNameSystem/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <armarx/navigation/client/NavigationStackConfig.h>
#include <armarx/navigation/client/Navigator.h>
#include <armarx/navigation/client/PathBuilder.h>
#include <armarx/navigation/client/services/IceNavigator.h>
#include <armarx/navigation/client/services/MemorySubscriber.h>
#include <armarx/navigation/client/services/SimpleEventHandler.h>
#include <armarx/navigation/client/types.h>
#include <armarx/navigation/global_planning/AStar.h>
#include <armarx/navigation/trajectory_control/TrajectoryController.h>
#include <armarx/navigation/trajectory_control/TrajectoryFollowingController.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

namespace armarx::navigation::statecharts::navigation_group
{
    // DO NOT EDIT NEXT LINE
    NavigateToLocation::SubClassRegistry
        NavigateToLocation::Registry(NavigateToLocation::GetName(),
                                     &NavigateToLocation::CreateInstance);

    void
    NavigateToLocation::onEnter()
    {
        ARMARX_INFO << "onEnter ...";
        // put your user code for the enter-point here
        // execution time should be short (<100ms)
    }

    void
    NavigateToLocation::run()
    {
        ARMARX_IMPORTANT << "Navigating to location `" << in.getlocation() << "`";

        // parameterize the navigation stack
        client::NavigationStackConfig cfg;
        cfg.general(client::GeneralConfig{});
        cfg.globalPlanner(armarx::navigation::global_planning::AStarParams{});
        cfg.trajectoryController(
            armarx::navigation::traj_ctrl::TrajectoryFollowingControllerParams{});

        // configure the `navigator` which provides a simplified and typed interface to the navigation server
        client::IceNavigator iceNavigator(getNavigator());

        armem::client::MemoryNameSystem mns(getMemoryNameSystem());

        client::MemorySubscriber memorySubscriber(GetName(), mns);

        // register our config
        ARMARX_INFO << "Registering config";
        iceNavigator.createConfig(cfg, GetName());

        client::Navigator navigator{client::Navigator::InjectedServices{
            .navigator = &iceNavigator, .subscriber = &memorySubscriber}};

        // assemble the path, which might consist of waypoints and a goal (the goal is just the last `waypoint`)
        client::PathBuilder builder;
        builder.add(in.getlocation(), client::GlobalPlanningStrategy::Point2Point);

        // execute
        ARMARX_INFO << "Sending navigation request";
        navigator.moveTo(builder, core::NavigationFrame::Absolute);

        // Wait until goal is reached
        ARMARX_INFO << "Waiting until goal is reached.";
        client::StopEvent se = navigator.waitForStop();
        if (se)
        {
            ARMARX_INFO << "Goal `" << in.getlocation() << "`reached.";
        }
        else
        {
            if (se.isSafetyStopTriggeredEvent())
            {
                ARMARX_ERROR << "Safety stop was triggered!";
                emitFailure();
            }
            else if (se.isUserAbortTriggeredEvent())
            {
                ARMARX_ERROR << "Aborted by user!";
                emitFailure();
            }
            else if (se.isInternalErrorEvent())
            {
                ARMARX_ERROR << "Unknown internal error occured! "
                             << se.toInternalErrorEvent().message;
                emitFailure();
            }
        }

        emitSuccess();
    }

    //void NavigateToLocation::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void
    NavigateToLocation::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr
    NavigateToLocation::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new NavigateToLocation(stateData));
    }

} // namespace armarx::navigation::statecharts::navigation_group
