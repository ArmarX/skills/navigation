/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    navigation::NavigationGroup::NavigationGroupRemoteStateOfferer
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "NavigationGroupRemoteStateOfferer.h"

namespace armarx::navigation::statecharts::navigation_group
{
    // DO NOT EDIT NEXT LINE
    NavigationGroupRemoteStateOfferer::SubClassRegistry NavigationGroupRemoteStateOfferer::Registry(
        NavigationGroupRemoteStateOfferer::GetName(),
        &NavigationGroupRemoteStateOfferer::CreateInstance);

    NavigationGroupRemoteStateOfferer::NavigationGroupRemoteStateOfferer(
        StatechartGroupXmlReaderPtr reader) :
        XMLRemoteStateOfferer<NavigationGroupStatechartContext>(reader)
    {
    }

    void
    NavigationGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
    {
    }

    void
    NavigationGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
    {
    }

    void
    NavigationGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
    {
    }

    // DO NOT EDIT NEXT FUNCTION
    std::string
    NavigationGroupRemoteStateOfferer::GetName()
    {
        return "NavigationGroupRemoteStateOfferer";
    }

    // DO NOT EDIT NEXT FUNCTION
    XMLStateOffererFactoryBasePtr
    NavigationGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
    {
        return XMLStateOffererFactoryBasePtr(new NavigationGroupRemoteStateOfferer(reader));
    }
} // namespace armarx::navigation::statecharts::navigation_group
