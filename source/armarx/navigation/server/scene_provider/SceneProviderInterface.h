/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/time/DateTime.h>

namespace armarx::navigation::core
{
    struct Scene;
}

namespace armarx::navigation::server::scene_provider
{

    class SceneProviderInterface
    {
    public:
        virtual const core::Scene& scene() const = 0;

        virtual bool initialize(const DateTime& timestamp) = 0;
        virtual bool synchronize(const DateTime& timestamp) = 0;


        // non-api
        virtual ~SceneProviderInterface() = default;
    };
} // namespace armarx::navigation::server::scene_provider
