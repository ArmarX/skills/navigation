#pragma once

// RobotAPI
#include <RobotAPI/interface/units/PlatformUnitInterface.h>

// Navigation
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/server/execution/ExecutorInterface.h>

namespace armarx::navigation::server
{

    /**
     * @brief The PlatformUnitExecutor class
     *
     * TODO: Should be renamed to whatever the new unit will be called which takes Mat4f
     * as input instead of X/Y/Yaw, so that we have a generic interface.
     */
    class PlatformUnitExecutor : virtual public ExecutorInterface
    {

    public:
        PlatformUnitExecutor(PlatformUnitInterfacePrx platformUnit);
        ~PlatformUnitExecutor() override;

        void move(const core::Twist& twist) override;

    private:
        PlatformUnitInterfacePrx platformUnit;
    };

} // namespace armarx::navigation::server
