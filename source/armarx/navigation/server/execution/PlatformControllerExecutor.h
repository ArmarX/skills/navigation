#pragma once

#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

#include <armarx/control/client/ComponentPlugin.h>
#include <armarx/navigation/common/controller_types.h>
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/platform_controller/aron/PlatformTrajectoryControllerConfig.aron.generated.h>
#include <armarx/navigation/platform_controller/controller_descriptions.h>
#include <armarx/navigation/server/execution/ExecutorInterface.h>

namespace armarx::control::client
{
    class ComponentPlugin;
}

namespace armarx::navigation::server
{

    /**
     * @brief The PlatformUnitExecutor class
     *
     * TODO: Should be renamed to whatever the new unit will be called which takes Mat4f
     * as input instead of X/Y/Yaw, so that we have a generic interface.
     */
    class PlatformControllerExecutor : virtual public ExecutorInterface
    {

    public:
        using ControllerComponentPlugin = armarx::control::client::ComponentPlugin;

        PlatformControllerExecutor(ControllerComponentPlugin& controllerComponentPlugin);
        ~PlatformControllerExecutor() override;

        void execute(const core::Trajectory& trajectory) override;

        void start() override;
        void stop() override;

    private:
        std::optional<armarx::control::client::ControllerWrapper<
            armarx::navigation::common::ControllerType::PlatformTrajectory>>
            ctrl;

        ControllerComponentPlugin& controllerPlugin;
    };

} // namespace armarx::navigation::server
