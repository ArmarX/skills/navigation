#pragma once

namespace armarx::navigation::core
{
    class Trajectory;
} // namespace armarx::navigation::core

namespace armarx::navigation::server
{

    /**
     * @brief An executer the server navigator will use to send its control commands to.
     */
    class ExecutorInterface
    {

    public:
        virtual ~ExecutorInterface() = default;

        virtual void execute(const core::Trajectory& trajectory) = 0;
        
        virtual void start() = 0;
        virtual void stop() = 0;
    };

} // namespace armarx::navigation::server
