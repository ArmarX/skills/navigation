#pragma once

#include <armarx/navigation/memory/client/events/Writer.h>
#include <armarx/navigation/memory/client/stack_result/Writer.h>
#include <armarx/navigation/server/event_publishing/EventPublishingInterface.h>


namespace armarx::navigation::server
{

    class MemoryPublisher : virtual public EventPublishingInterface
    {

    public:
        MemoryPublisher(armarx::navigation::memory::client::stack_result::Writer* resultWriter,
                        armarx::navigation::memory::client::events::Writer* eventsWriter,
                        const std::string& clientId);

        void globalTrajectoryUpdated(const global_planning::GlobalPlannerResult& res) override;
        void localTrajectoryUpdated(const loc_plan::LocalPlannerResult& res) override;
        void trajectoryControllerUpdated(const traj_ctrl::TrajectoryControllerResult& res) override;

        void globalPlanningFailed(const core::GlobalPlanningFailedEvent& event) override;

        void movementStarted(const core::MovementStartedEvent& event) override;
        void goalReached(const core::GoalReachedEvent& event) override;

        void waypointReached(const core::WaypointReachedEvent& event) override;

        void safetyThrottlingTriggered(const core::SafetyThrottlingTriggeredEvent& event) override;

        void safetyStopTriggered(const core::SafetyStopTriggeredEvent& event) override;

        void userAbortTriggered(const core::UserAbortTriggeredEvent& event) override;

        void internalError(const core::InternalErrorEvent& event) override;

        // Non-API.
    public:
        ~MemoryPublisher() override = default;

    private:
        armarx::navigation::memory::client::stack_result::Writer* resultWriter;
        armarx::navigation::memory::client::events::Writer* eventsWriter;

        const std::string clientId;
    };

} // namespace armarx::navigation::server
