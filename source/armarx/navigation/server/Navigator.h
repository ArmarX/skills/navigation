/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <atomic>
#include <optional>

// Eigen
#include <Eigen/Core>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

// Navigation
#include "monitoring/GoalReachedMonitor.h"
#include <armarx/navigation/client/ice_conversions.h>
#include <armarx/navigation/core/NavigatorInterface.h>
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/global_planning/GlobalPlanner.h>
#include <armarx/navigation/server/GraphBuilder.h>
#include <armarx/navigation/server/NavigationStack.h>
#include <armarx/navigation/server/StackResult.h>
#include <armarx/navigation/server/event_publishing/EventPublishingInterface.h>
#include <armarx/navigation/server/execution/ExecutorInterface.h>
#include <armarx/navigation/server/introspection/IntrospectorInterface.h>
#include <armarx/navigation/server/monitoring/GoalReachedMonitor.h>
#include <armarx/navigation/trajectory_control/TrajectoryController.h>

namespace armarx::navigation::server
{

    namespace scene_provider
    {
        class SceneProviderInterface;
    }

    class Navigator : virtual public core::NavigatorInterface
    {

    public:
        struct Config
        {
            struct General
            {
                struct
                {
                    int replanningUpdatePeriod{100}; // [ms]
                } tasks;
            };

            server::NavigationStack stack;
            General general;
        };

        struct InjectedServices
        {
            ExecutorInterface* executor;
            EventPublishingInterface* publisher;
            IntrospectorInterface* introspector = nullptr;

            scene_provider::SceneProviderInterface* sceneProvider;
        };

        Navigator(const Config& config, const InjectedServices& services);

        void moveTo(const std::vector<core::Pose>& waypoints,
                    core::NavigationFrame navigationFrame) override;


        void moveTo(const std::vector<client::WaypointTarget>& targets,
                    core::NavigationFrame navigationFrame) override;

        void moveTowards(const core::Direction& direction,
                         core::NavigationFrame navigationFrame) override;

        void pause() override;

        void resume() override;

        bool isPaused() const noexcept override;

        void stop() override;

        bool isStopped() const noexcept override;

        // Non-API
    public:
        Navigator(Navigator&& other) noexcept;

        ~Navigator() override;

    private:
        void moveToAbsolute(const std::vector<core::Pose>& waypoints);
        void moveTowardsAbsolute(const core::Direction& direction);

        void run();

        void updateScene();

        std::optional<loc_plan::LocalPlannerResult> updateLocalPlanner();
        void updateExecutor(const std::optional<loc_plan::LocalPlannerResult>& localPlan);
        void updateIntrospector(const std::optional<loc_plan::LocalPlannerResult>& localPlan);

        void updateMonitor();

        // const core::Trajectory& currentTrajectory() const;
        // bool isStackResultValid() const noexcept;

        void stopAllThreads();

        void startStack();

        // helper methods
        void setGraphEdgeCosts(core::Graph& graph) const;
        GraphBuilder convertToGraph(const std::vector<client::WaypointTarget>& targets) const;

        bool hasLocalPlanner() const noexcept
        {
            return config.stack.localPlanner != nullptr;
        }

        Config config;

        InjectedServices srv;

        std::atomic_bool executorEnabled = true;

        PeriodicTask<Navigator>::pointer_type runningTask;

        std::optional<GoalReachedMonitor> goalReachedMonitor;


        std::optional<global_planning::GlobalPlannerResult> globalPlan;
        std::optional<loc_plan::LocalPlannerResult> localPlan;
    };

} // namespace armarx::navigation::server
