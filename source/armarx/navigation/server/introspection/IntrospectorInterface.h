/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <armarx/navigation/global_planning/GlobalPlanner.h>
#include <armarx/navigation/local_planning/LocalPlanner.h>
#include <armarx/navigation/safety_control/SafetyController.h>
#include <armarx/navigation/trajectory_control/TrajectoryController.h>

namespace armarx::navigation::server
{

    struct StackResult;

    class IntrospectorInterface
    {
    public:
        virtual ~IntrospectorInterface() = default;

        virtual void onGoal(const core::Pose& goal) = 0;

        virtual void success() = 0;
        virtual void failure() = 0;

        virtual void onGlobalPlannerResult(const global_planning::GlobalPlannerResult& result) = 0;
        virtual void onLocalPlannerResult(const loc_plan::LocalPlannerResult& result) = 0;

        virtual void onGlobalShortestPath(const std::vector<core::Pose>& path) = 0;

        virtual void onGlobalGraph(const core::Graph& graph) = 0;

    protected:
    private:
    };

} // namespace armarx::navigation::server
