/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <cstdint>
#include <vector>

#include <SemanticObjectRelations/Shapes/Shape.h>
#include <armarx/navigation/client/PathBuilder.h>
#include <armarx/navigation/core/Graph.h>
#include <armarx/navigation/core/events.h>

namespace armarx::navigation::server
{


    class GraphBuilder
    {
    public:
        void initialize(const core::Pose& pose);

        // const std::vector<core::Graph::VertexDescriptor>& activeVertices() const;

        void connect(const core::Pose& pose, const client::GlobalPlanningStrategy& strategy);


        // which type to choose? this is a sequence of chains

        /**
         * @brief
         *
         * @param routes
         * @param strategy strategy on how to connect the last vertex and the routes
         */
        void connect(const std::vector<core::Graph::VertexDescriptor>& routes,
                     const client::GlobalPlanningStrategy& strategy);
        void connect(const std::vector<core::GraphPath>& routes,
                     const client::GlobalPlanningStrategy& strategy);
        // void connect(const core::Pose& pose, const client::GlobalPlanningStrategy& strategy);

        const core::Graph& getGraph() const;

        std::optional<core::Graph::ConstVertex> startVertex;

        core::Graph::VertexDescriptor goalVertex() const;

        core::Pose goalPose() const;

    private:
        // void addEdge(const std::vector<core::Graph::VertexDescriptor>& sources,
        //              const std::vector<core::Graph::VertexDescriptor>& targets);

        void validate(auto&& vertices) const;

        semrel::ShapeID addOrGetVertex(const core::Graph::VertexAttrib& attributes);
        std::optional<semrel::ShapeID>
        getVertexByAttributes(const core::Graph::VertexAttrib& attributes);

        std::vector<semrel::ShapeID> activeVertices_;

        core::Graph graph;

        int64_t vertexIdCnt = -1;
    };

} // namespace armarx::navigation::server
