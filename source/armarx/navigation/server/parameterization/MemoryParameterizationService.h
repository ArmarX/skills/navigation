/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>

#include <armarx/navigation/core/types.h>
#include <armarx/navigation/memory/client/parameterization/Reader.h>
#include <armarx/navigation/memory/client/parameterization/Writer.h>


namespace armarx::navigation::server
{


    class MemoryParameterizationService
    {
    public:
        MemoryParameterizationService(memory::client::param::Reader* reader,
                                      memory::client::param::Writer* writer);

        bool store(const aron::data::DictPtr& params,
                   const std::string& clientId,
                   const armarx::core::time::DateTime& timestamp);

        bool store(const aron::data::dto::DictPtr& params,
                   const std::string& clientId,
                   const armarx::core::time::DateTime& timestamp);


    protected:
    private:
        memory::client::param::Reader* reader;
        memory::client::param::Writer* writer;
    };
} // namespace armarx::navigation::server
