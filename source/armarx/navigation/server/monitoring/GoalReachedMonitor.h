/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <cmath>
#include <vector>

#include <VirtualRobot/MathTools.h>

#include <armarx/navigation/core/types.h>

namespace armarx::navigation::server
{

    struct GoalReachedMonitorConfig
    {
        float posTh{70.F};                                  // [mm]
        float oriTh{VirtualRobot::MathTools::deg2rad(5.F)}; // [rad]

        float linearVelTh{100.F};                                  // [mm/s]
        float angularVelTh{VirtualRobot::MathTools::deg2rad(5.F)}; // [rad/s]
    };

    class GoalReachedMonitor
    {
    public:
        GoalReachedMonitor(const core::Pose& goal,
                           const core::Scene& scene,
                           const GoalReachedMonitorConfig& config);
        virtual ~GoalReachedMonitor() = default;

        GoalReachedMonitor(GoalReachedMonitor&& other) noexcept;
        GoalReachedMonitor& operator=(GoalReachedMonitor&&) noexcept;

        bool goalReached() const noexcept;

        const core::Pose& goal() const noexcept;

    private:
        const core::Pose goal_;
        const core::Scene& scene;
        const GoalReachedMonitorConfig config;
    };

} // namespace armarx::navigation::server
