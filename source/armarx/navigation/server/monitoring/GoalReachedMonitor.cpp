#include "GoalReachedMonitor.h"

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>


namespace armarx::navigation::server
{

    GoalReachedMonitor::GoalReachedMonitor(const core::Pose& goal,
                                           const core::Scene& scene,
                                           const GoalReachedMonitorConfig& config) :
        goal_(goal), scene(scene), config(config)
    {
        //ARMARX_CHECK(not waypoints.empty());
    }

    bool
    GoalReachedMonitor::goalReached() const noexcept
    {
        // return false;
        ARMARX_TRACE;

        const core::Pose world_T_robot(scene.robot->getGlobalPose());
        const core::Pose& world_T_goal(goal_);

        const core::Pose robot_T_goal = world_T_robot.inverse() * world_T_goal;

        const bool posReached = robot_T_goal.translation().norm() <= config.posTh;
        const bool oriReached = Eigen::AngleAxisf(robot_T_goal.linear()).angle() <= config.oriTh;

        if (posReached)
        {
            ARMARX_DEBUG << "Position goal `" << robot_T_goal.translation().head<2>()
                         << "` reached";
        }

        if (oriReached)
        {
            ARMARX_DEBUG << "Orientation goal reached";
        }

        // FIXME: 
        // if (not scene.platformVelocity.has_value())
        // {
        //     ARMARX_DEBUG << "Platform velocity not available yet.";
        //     return false;
        // }

        // const bool linearVelocityLow = scene.platformVelocity->linear.norm() < config.linearVelTh;
        // const bool angularVelocityLow =
        //     scene.platformVelocity->angular.norm() < config.angularVelTh;

        // const bool velocityLow = linearVelocityLow and angularVelocityLow;

        // if (velocityLow)
        // {
        //     ARMARX_DEBUG << "Robot has come to rest.";
        // }

        return posReached and oriReached; //and velocityLow;
    }

    const core::Pose&
    GoalReachedMonitor::goal() const noexcept
    {
        return goal_;
    }

    GoalReachedMonitor::GoalReachedMonitor(GoalReachedMonitor&& other) noexcept :
        goal_{other.goal_}, scene{other.scene}, config(other.config)
    {
    }

    GoalReachedMonitor&
    GoalReachedMonitor::operator=(GoalReachedMonitor&& /*unused*/) noexcept
    {
        return *this;
    }
} // namespace armarx::navigation::server
