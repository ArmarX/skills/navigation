/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::server
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "armarx/navigation/server/scene_provider/SceneProvider.h"
#include <armarx/navigation/client/NavigationStackConfig.h>
#include <armarx/navigation/client/services/SimpleEventHandler.h>
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/factories/NavigationStackFactory.h>
#include <armarx/navigation/global_planning/AStar.h>
#include <armarx/navigation/global_planning/Point2Point.h>
#include <armarx/navigation/server/NavigationStack.h>
#include <armarx/navigation/server/Navigator.h>
#include <armarx/navigation/server/execution/DummyExecutor.h>
#include <armarx/navigation/trajectory_control/TrajectoryFollowingController.h>

#define BOOST_TEST_MODULE Navigation::ArmarXLibraries::server

#define ARMARX_BOOST_TEST

#include <iostream>

#include <VirtualRobot/Robot.h>

#include <armarx/navigation/Test.h>

using namespace armarx::navigation;

namespace armarx::navigation::server::scene_provider
{

    class DummySceneProvider : public SceneProviderInterface
    {
    public:
        DummySceneProvider() = default;

        const core::Scene&
        scene() const override
        {
            return dummyScene; 
        };

        bool
        initialize(const armarx::DateTime&  /*timestamp*/) override
        {
            return true;
        };

        bool
        synchronize(const armarx::DateTime&  /*timestamp*/) override
        {
            return true;
        };


        // non-api
        ~DummySceneProvider() override = default;

        private:
        core::Scene dummyScene; // FIXME implement
    };

} // namespace armarx::navigation::server::scene_provider

BOOST_AUTO_TEST_CASE(testNavigator)
{

    const core::Pose goal = core::Pose::Identity();

    // create Navigator
    core::Scene scene;
    scene.robot = std::make_shared<VirtualRobot::LocalRobot>("foo", "bar");
    scene.robot->setGlobalPose(Eigen::Matrix4f::Identity(), false);

    // TODO create static shared ctor
    server::NavigationStack stack{.globalPlanner = std::make_shared<global_planning::Point2Point>(
                                      global_planning::Point2PointParams(), scene)};
    //   .trajectoryControl =
    //       std::make_shared<traj_ctrl::TrajectoryFollowingController>(
    //           traj_ctrl::TrajectoryFollowingControllerParams(), scene)};

    // Executor.
    server::DummyExecutor executor{scene.robot, server::DummyExecutor::Params()};
    client::SimpleEventHandler eventHandler;

    server::scene_provider::DummySceneProvider sceneProvider;

    server::Navigator navigator(
        server::Navigator::Config{.stack = stack, .general = server::Navigator::Config::General{}},
        server::Navigator::InjectedServices{
            .executor = &executor, .publisher = &eventHandler, .sceneProvider = &sceneProvider});
    navigator.moveTo(std::vector{goal}, core::NavigationFrame::Absolute);

    BOOST_CHECK_EQUAL(true, true);
}

/**
BOOST_AUTO_TEST_CASE(testNavigatorWithFactory)
{

    // create navigation stack config
    client::NavigationStackConfig cfg;
    cfg.configureGlobalPlanner(global_planning::AStarParams());
    cfg.configureTrajectoryController(traj_ctrl::TrajectoryFollowingControllerParams());

    const auto stackConfig = cfg.toAron();
    const core::Pose goal = core::Pose::Identity();

    // here, we would send data over Ice ...

    // NavigatorPrx navigator;
    // navigator->moveTo(goal, stackConfig, core::NavigationFrame::Absolute);
    // navigator->moveTorwards(direction, stackConfig, core::NavigationFrame::Relative);

    // CLIENT
    ////////////////////////////////////////////////
    // SERVER

    // create Navigator
    core::Context ctx;
    server::NavigationStack stack = fac::NavigationStackFactory::create(stackConfig, ctx);

    server::Navigator navigator(stack, ctx);
    navigator.moveTo(goal, core::NavigationFrames::Absolute);

    BOOST_CHECK_EQUAL(true, true);
}*/
