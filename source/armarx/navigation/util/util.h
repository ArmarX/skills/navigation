/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>
#include <RobotAPI/libraries/armem_vision/OccupancyGridHelper.h>
#include <RobotAPI/libraries/armem_vision/types.h>

namespace armarx::navigation::util
{
    objpose::ObjectPoseSeq filterObjects(objpose::ObjectPoseSeq objects,
                                         const std::vector<std::string>& datasetBlacklist);

    std::optional<objpose::ObjectPose> findObject(const objpose::ObjectPoseSeq& objectPoses,
                                                  const armarx::ObjectID& objectID);

    VirtualRobot::ManipulationObjectPtr asManipulationObject(const objpose::ObjectPose& objectPose);
    VirtualRobot::SceneObjectSetPtr asSceneObjects(const objpose::ObjectPoseSeq& objectPoses);
    VirtualRobot::SceneObjectSetPtr asSceneObjects(const armem::vision::OccupancyGrid& occupancyGrid,
                                                   const OccupancyGridHelper::Params& params);

} // namespace armarx::navigation::util
