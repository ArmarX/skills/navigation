/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "persistence.h"

#include <cmath>
#include <cstddef>
#include <cstdint>
#include <filesystem>
#include <fstream>

#include <opencv2/core.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/opencv.hpp>

#include <SimoxUtility/json/json.hpp>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <armarx/navigation/algorithms/Costmap.h>
#include <armarx/navigation/algorithms/types.h>

namespace armarx::navigation::algorithms
{

    Costmap
    load(const std::filesystem::path& directory)
    {
        // load
        std::ifstream ifs(directory / "costmap.json");
        const nlohmann::json j = nlohmann::json::parse(ifs);

        // params
        const auto& jParam = j["params"];

        const Costmap::Parameters params{.binaryGrid = jParam["binary_grid"],
                                         .cellSize = jParam["cell_size"]};

        // scene bounds
        const auto& jSceneBounds = j["scene_bounds"];

        const std::vector<float> boundsMin = jSceneBounds["min"];
        const std::vector<float> boundsMax = jSceneBounds["max"];

        ARMARX_CHECK_EQUAL(boundsMin.size(), 2);
        ARMARX_CHECK_EQUAL(boundsMax.size(), 2);

        const SceneBounds sceneBounds{.min = {boundsMin.at(0), boundsMin.at(1)},
                                      .max = {boundsMax.at(0), boundsMax.at(1)}};


        // grid
        const std::string gridFilename = j["grid_filename"];
        cv::Mat gridMat = cv::imread((directory / gridFilename).string(), cv::IMREAD_ANYCOLOR | cv::IMREAD_ANYDEPTH);

        Eigen::MatrixXf grid;
        cv::cv2eigen(gridMat, grid);

        // mask, if any
        std::optional<Costmap::Mask> optMask;
        if (not j["mask_filename"].empty())
        {
            const std::string maskFilename = j["grid_filename"];
            cv::Mat maskMat = cv::imread((directory / maskFilename).string(), cv::IMREAD_ANYCOLOR | cv::IMREAD_ANYDEPTH);

            Eigen::Matrix<std::uint8_t, Eigen::Dynamic, Eigen::Dynamic> mask;
            cv::cv2eigen(maskMat, mask);

            optMask = mask.cast<bool>();
        }

        return {grid, params, sceneBounds, optMask};
    }


    bool
    save(const Costmap& costmap, const std::filesystem::path& directory)
    {
        ARMARX_TRACE;

        ARMARX_INFO << "Storing costmap in directory `" << directory.string() << "`";

        if (not std::filesystem::exists(directory))
        {
            ARMARX_INFO << "Creating directory `" << directory.string() << "`";
            std::filesystem::create_directories(directory);
        }

        nlohmann::json j;

        // params
        {
            auto& jParam = j["params"];
            jParam["binary_grid"] = costmap.params().binaryGrid;
            jParam["cell_size"] = costmap.params().cellSize;
        }

        // scene bounds
        {
            auto& jSceneBounds = j["scene_bounds"];
            jSceneBounds["min"] = std::vector<float>{costmap.getLocalSceneBounds().min.x(),
                                                     costmap.getLocalSceneBounds().min.y()};
            jSceneBounds["max"] = std::vector<float>{costmap.getLocalSceneBounds().max.x(),
                                                     costmap.getLocalSceneBounds().max.y()};
        }

        // grid
        {
            ARMARX_TRACE;

            cv::Mat1f grid;
            cv::eigen2cv(costmap.getGrid(), grid);

            const std::string gridFilename = "grid.exr";
            cv::imwrite((directory / gridFilename).string(), grid);

            j["grid_filename"] = gridFilename;

            // for debugging purpose, also save a png image
            const std::string gridDebuggingFilename = "grid.png";

            cv::Mat gridDebug;
            grid.convertTo(gridDebug, CV_16UC1);
            cv::imwrite((directory / gridFilename).string(), gridDebug);
        }

        // mask, if any
        {
            ARMARX_TRACE;

            if (costmap.getMask().has_value())
            {
                Eigen::Matrix<std::uint8_t, Eigen::Dynamic, Eigen::Dynamic> maskI =
                    costmap.getMask()->cast<std::uint8_t>();

                cv::Mat1b mask;
                cv::eigen2cv(maskI, mask);

                const std::string maskFilename = "mask.ppm";
                cv::imwrite((directory / maskFilename).string(), mask);

                j["mask_filename"] = maskFilename;
            }
        }

        // save
        std::ofstream of(directory / "costmap.json");
        of << j;

        return true;
    }


} // namespace armarx::navigation::algorithms
