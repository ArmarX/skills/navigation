#pragma once

#include <optional>

#include <Eigen/Core>

#include <armarx/navigation/algorithms/types.h>
#include <armarx/navigation/core/basic_types.h>

namespace armarx::navigation::algorithms
{

    class Costmap
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        friend class CostmapBuilder;

        struct Parameters
        {
            // if set to false, distance to obstacles will be computed and not only a binary collision check
            bool binaryGrid{false};

            /// How big each cell is in the uniform grid.
            float cellSize = 100.f;
        };


        using Index = Eigen::Array2i;
        using Position = Eigen::Vector2f;
        using Grid = Eigen::MatrixXf;

        // if 0, the cell is invalid
        using Mask = Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic>;

        Costmap(const Grid& grid,
                const Parameters& parameters,
                const SceneBounds& sceneBounds,
                const std::optional<Mask>& mask = std::nullopt,
                const core::Pose2D& origin = core::Pose2D::Identity());

        struct Vertex
        {
            Index index; // row corresponds to y; column corresponds to x
            Position position;
        };

        Position toPositionLocal(const Index& index) const;
        Position toPositionGlobal(const Index& index) const;
        Vertex toVertex(const Position& globalPosition) const;

        const Grid& getGrid() const;
        Grid&
        getMutableGrid()
        {
            return grid;
        }

        bool isInCollision(const Position& p) const;

        const Parameters& params() const noexcept;

        // get the scene bounds in the costmap's base frame (aka 'origin')
        const SceneBounds& getLocalSceneBounds() const noexcept;

        core::Pose2D
        centerPose() const
        {
            const Eigen::Vector2f costmap_P_center{
                (getLocalSceneBounds().max.x() - getLocalSceneBounds().min.x())/2 + getLocalSceneBounds().min.x(),
                (getLocalSceneBounds().max.y() - getLocalSceneBounds().min.y())/2 + getLocalSceneBounds().min.y()};

            return global_T_costmap * Eigen::Translation2f(costmap_P_center);
        }

        struct Optimum
        {
            float value;
            Index index;
            Position position;
        };

        Optimum optimum() const;

        bool add(const Costmap& other, float weight = 1.0);

        enum class AddMode
        {
            MIN,
            MAX
        };

        bool add(const Costmap& other, AddMode mode);

        //! checks whether the cell is masked out
        bool isValid(const Index& index) const noexcept;

        const std::optional<Mask>& getMask() const noexcept;

        std::optional<Costmap::Mask>& getMutableMask() noexcept;


        std::optional<float> value(const Index& index) const;

        std::optional<float> value(const Position& position) const;

        Costmap mergeInto(const std::vector<Costmap>& costmaps,
                          const std::vector<float>& weights) const;

        const core::Pose2D&
        origin() const
        {
            return global_T_costmap;
        }

        void
        setOrigin(const core::Pose2D& globalPose)
        {
            global_T_costmap = globalPose;
        }

    private:
        void validateSizes() const;

        Grid grid;
        std::optional<Mask> mask;

        const SceneBounds sceneBounds;

        const Parameters parameters;

        core::Pose2D global_T_costmap = core::Pose2D::Identity();
    };


} // namespace armarx::navigation::algorithms
