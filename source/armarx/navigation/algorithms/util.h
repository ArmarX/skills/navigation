
/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/Workspace/WorkspaceGrid.h>

#include <armarx/navigation/algorithms/Costmap.h>
#include <armarx/navigation/algorithms/types.h>


namespace armarx::navigation::algorithms
{
    SceneBounds computeSceneBounds(const VirtualRobot::SceneObjectSetPtr& obstacles);

    SceneBounds toSceneBounds(const VirtualRobot::WorkspaceGrid::Extends& extends);

    SceneBounds merge(const std::vector<SceneBounds>& sceneBounds);


    Costmap toCostmap(const VirtualRobot::WorkspaceGrid& workspaceGrid);


    // Costmap mergeUnaligned(const std::vector<Costmap>& costmaps,
    //                        const std::vector<float>& weights,
    //                        float cellSize = -1,
    //                        const Eigen::Array2f& offset = Eigen::Array2f::Zero());

    void checkSameSize(const std::vector<Costmap>& costmaps);

    /**
    * @brief Defines how the 
    * 
    */
    enum class CostmapMergeMode
    {
        MAX,
        MIN,
        AVERAGE
    };

    Costmap mergeAligned(const std::vector<Costmap>& costmaps, CostmapMergeMode mode);
    Costmap mergeAligned(const std::vector<Costmap>& costmaps, const std::vector<float>& weights);

    Costmap scaleCostmap(const Costmap& costmap, float cellSize);

} // namespace armarx::navigation::algorithms
