/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <armarx/navigation/algorithms/Costmap.h>
#include <armarx/navigation/algorithms/aron/Costmap.aron.generated.h>

namespace armarx::navigation::algorithms
{
    // TODO fix ADL and remove this function
    template <class DtoT, class BoT>
    DtoT
    toAron(const BoT& bo)
    {
        DtoT dto;
        toAron(dto, bo);
        return dto;
    }

    // aron xml definition is not complete. "grid" and "mask" fields will be added
    armarx::aron::data::DictPtr toAron(const Costmap& bo);

    // Costmap does not provide a default c'tor
    Costmap fromAron(const armem::wm::EntityInstance& entityInstance);


} // namespace armarx::navigation::algorithms
