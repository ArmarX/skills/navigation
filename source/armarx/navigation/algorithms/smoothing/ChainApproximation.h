/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>

namespace armarx::navigation::algorithm
{

    namespace detail
    {
        struct ChainApproximationParams
        {
            float distanceTh; // [mm]

            int maxIterations{-1};
        };

        struct ApproximationResult
        {
            enum class TerminationCondition
            {
                Converged,
                IterationLimit
            };
            TerminationCondition condition;

            int iterations;

            float reductionFactor{0.F};
        };

        std::ostream& operator<<(std::ostream& str, const ApproximationResult& res);
    } // namespace detail

    class ChainApproximation
    {
    public:
        using Point = Eigen::Vector2f;
        using Points = std::vector<Point>;

        using Params = detail::ChainApproximationParams;
        using ApproximationResult = detail::ApproximationResult;

        ChainApproximation(const Points& points, const Params& params);

        ApproximationResult approximate();

        [[nodiscard]] Points approximatedChain() const;

    private:
        struct Triplet
        {
            Triplet(const int& a, const int& b, const int& c) : a(a), b(b), c(c)
            {
            }

            int a;
            int b;
            int c;
        };

        using Triplets = std::vector<Triplet>;

        Triplets getTriplets() const;

        std::vector<float> computeDistances(const Triplets& triplets);
        float computeDistance(const Triplet& triplet) const;

        bool approximateStep();

        Points points;

        std::vector<int> indices;

        const Params params;
    };

} // namespace armarx::navigation::algorithm
