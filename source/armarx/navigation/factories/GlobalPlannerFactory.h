/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/aron/core/data/variant/forward_declarations.h>

#include <armarx/navigation/core/fwd.h>
#include <armarx/navigation/factories/fwd.h>

namespace armarx::navigation::fac
{
    /**
    * @brief Factory to create a global_planning::GlobalPlanner
    *
    */
    class GlobalPlannerFactory
    {
    public:
        static global_planning::GlobalPlannerPtr create(const aron::data::DictPtr& params,
                                                        const core::Scene& ctx);
    };

} // namespace armarx::navigation::fac
