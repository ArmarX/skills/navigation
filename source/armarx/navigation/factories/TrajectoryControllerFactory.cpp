#include "TrajectoryControllerFactory.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/aron/core/data/variant/primitive/String.h>

#include <armarx/navigation/core/constants.h>
#include <armarx/navigation/trajectory_control/TrajectoryController.h>
#include <armarx/navigation/trajectory_control/TrajectoryFollowingController.h>
#include <armarx/navigation/trajectory_control/WaypointController.h>

namespace armarx::navigation::fac
{
    traj_ctrl::TrajectoryControllerPtr
    TrajectoryControllerFactory::create(const VirtualRobot::RobotPtr& robot,
                                        const aron::data::DictPtr& params)
    {
        namespace layer = traj_ctrl;

        if (not params)
        {
            return nullptr;
        }

        // algo name
        const auto algoName = aron::data::String::DynamicCast(params->getElement(core::NAME_KEY));
        ARMARX_CHECK_NOT_NULL(algoName);
        const layer::Algorithms algo = layer::AlgorithmNames.from_name(algoName->getValue());

        // algo params
        const auto algoParams = aron::data::Dict::DynamicCast(params->getElement(core::PARAMS_KEY));
        ARMARX_CHECK_NOT_NULL(algoParams);

        traj_ctrl::TrajectoryControllerPtr controller;
        switch (algo)
        {
            case traj_ctrl::Algorithms::WaypointController:
                controller = std::make_shared<traj_ctrl::WaypointController>(
                    traj_ctrl::WaypointControllerParams::FromAron(algoParams));
                break;
            case traj_ctrl::Algorithms::TrajectoryFollowingController:
                controller = std::make_shared<traj_ctrl::TrajectoryFollowingController>(
                    robot, traj_ctrl::TrajectoryFollowingControllerParams::FromAron(algoParams));
                break;
        }

        return controller;
    }
} // namespace armarx::navigation::fac
