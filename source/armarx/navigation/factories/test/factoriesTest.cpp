/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::factories
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <armarx/navigation/client/NavigationStackConfig.h>
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/global_planning/AStar.h>
#include <armarx/navigation/trajectory_control/TrajectoryFollowingController.h>
#define BOOST_TEST_MODULE Navigation::ArmarXLibraries::factories

#define ARMARX_BOOST_TEST

#include "../NavigationStackFactory.h"
#include <armarx/navigation/Test.h>


BOOST_AUTO_TEST_CASE(testGlobalPlannerCreation)
{
    // armarx::navigation::client::NavigationStackConfig cfg;
    // cfg.globalPlanner(armarx::navigation::global_planning::AStarParams())
    // .trajectoryController(armarx::navigation::traj_ctrl::TrajectoryFollowingController::Params());

    // const auto dto = cfg.toAron();

    // armarx::navigation::core::Scene scene; // TODO mock

    // const auto navStack = armarx::navigation::fac::NavigationStackFactory::create(dto, scene);

    // BOOST_CHECK(dynamic_cast<armarx::navigation::global_planning::AStar*>(navStack.globalPlanner.get()) != nullptr);
    // BOOST_CHECK(dynamic_cast<armarx::navigation::traj_ctrl::TrajectoryFollowingController*>(navStack.trajectoryControl.get()) != nullptr);
}


BOOST_AUTO_TEST_CASE(testStackCreation)
{
    armarx::navigation::client::NavigationStackConfig cfg;
    cfg.globalPlanner(armarx::navigation::global_planning::AStarParams())
        .trajectoryController(
            armarx::navigation::traj_ctrl::TrajectoryFollowingController::Params());

    const auto dto = cfg.toAron();

    armarx::navigation::core::Scene scene; // TODO mock

    const auto navStack = armarx::navigation::fac::NavigationStackFactory::create(dto, scene);

    BOOST_CHECK(navStack.globalPlanner.get() != nullptr);
    // BOOST_CHECK(navStack.trajectoryControl.get() != nullptr);

    BOOST_CHECK(dynamic_cast<armarx::navigation::global_planning::AStar*>(
                    navStack.globalPlanner.get()) != nullptr);
    // BOOST_CHECK(dynamic_cast<armarx::navigation::traj_ctrl::TrajectoryFollowingController*>(
    //                 navStack.trajectoryControl.get()) != nullptr);
}
