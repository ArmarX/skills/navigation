#include "LocalPlannerFactory.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/aron/core/data/variant/primitive/String.h>

#include <armarx/navigation/core/constants.h>
#include <armarx/navigation/local_planning/TimedElasticBands.h>

namespace armarx::navigation::fac
{
    loc_plan::LocalPlannerPtr
    LocalPlannerFactory::create(const aron::data::DictPtr& params, const core::Scene& ctx)
    {
        namespace layer = loc_plan;

        if (not params)
        {
            return nullptr;
        }

        // algo name
        const auto algoName = aron::data::String::DynamicCast(params->getElement(core::NAME_KEY));
        ARMARX_CHECK_NOT_NULL(algoName);
        const layer::Algorithms algo = layer::AlgorithmNames.from_name(algoName->getValue());

        // algo params
        const auto algoParams = aron::data::Dict::DynamicCast(params->getElement(core::PARAMS_KEY));
        ARMARX_CHECK_NOT_NULL(algoParams);

        loc_plan::LocalPlannerPtr localPlanner;
        switch (algo)
        {
            case loc_plan::Algorithms::TimedElasticBands:
                localPlanner = std::make_shared<loc_plan::TimedElasticBands>(
                    loc_plan::TimedElasticBandsParams::FromAron(algoParams), ctx);
                break;
        }

        return localPlanner;
    }
} // namespace armarx::navigation::fac
