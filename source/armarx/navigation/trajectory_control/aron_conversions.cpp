#include "aron_conversions.h"

#include <RobotAPI/libraries/aron/common/aron_conversions.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/core.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/stl.h>

#include <armarx/navigation/core/aron_conversions.h>
#include <armarx/navigation/trajectory_control/TrajectoryController.h>
#include <armarx/navigation/trajectory_control/TrajectoryFollowingController.h>
#include <armarx/navigation/trajectory_control/WaypointController.h>
#include <armarx/navigation/trajectory_control/aron/TrajectoryControllerParams.aron.generated.h>
#include <armarx/navigation/trajectory_control/aron/TrajectoryFollowingControllerParams.aron.generated.h>
#include <armarx/navigation/trajectory_control/aron/WaypointControllerParams.aron.generated.h>


namespace armarx::navigation::traj_ctrl
{

    void
    toAron(arondto::TrajectoryControllerParams& dto, const TrajectoryControllerParams& bo)
    {
        // core::toAron(dto.limits, bo.limits);
    }

    void
    fromAron(const arondto::TrajectoryControllerParams& dto, TrajectoryControllerParams& bo)
    {
        // core::fromAron(dto.limits, bo.limits);
    }

    void
    toAron(arondto::TrajectoryFollowingControllerParams& dto,
           const TrajectoryFollowingControllerParams& bo)
    {
        // toAron(static_cast<arondto::TrajectoryControllerParams&>(dto),
        //        static_cast<const TrajectoryControllerParams&>(bo));

        core::toAron(dto.pidOri, bo.pidOri);
        core::toAron(dto.pidPos, bo.pidPos);
        core::toAron(dto.limits, bo.limits);
    }

    void
    fromAron(const arondto::TrajectoryFollowingControllerParams& dto,
             TrajectoryFollowingControllerParams& bo)
    {
        // fromAron(static_cast<const arondto::TrajectoryControllerParams&>(dto),
        //          static_cast<TrajectoryControllerParams&>(bo));

        core::fromAron(dto.pidOri, bo.pidOri);
        core::fromAron(dto.pidPos, bo.pidPos);
        core::fromAron(dto.limits, bo.limits);
    }

    void
    toAron(arondto::WaypointControllerParams& dto, const WaypointControllerParams& bo)
    {
        toAron(static_cast<arondto::WaypointControllerParams&>(dto),
               static_cast<const WaypointControllerParams&>(bo));
    }

    void
    fromAron(const arondto::WaypointControllerParams& dto, WaypointControllerParams& bo)
    {
        fromAron(static_cast<const arondto::WaypointControllerParams&>(dto),
                 static_cast<WaypointControllerParams&>(bo));
    }


} // namespace armarx::navigation::traj_ctrl
