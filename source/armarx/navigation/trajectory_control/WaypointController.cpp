#include "WaypointController.h"

#include <armarx/navigation/trajectory_control/TrajectoryController.h>
#include <armarx/navigation/trajectory_control/aron/WaypointControllerParams.aron.generated.h>
#include <armarx/navigation/trajectory_control/aron_conversions.h>
#include <armarx/navigation/trajectory_control/core.h>

namespace armarx::navigation::traj_ctrl
{
    // WaypointControllerParams

    Algorithms
    WaypointControllerParams::algorithm() const
    {
        return Algorithms::WaypointController;
    }

    aron::data::DictPtr
    WaypointControllerParams::toAron() const
    {
        arondto::WaypointControllerParams dto;

        WaypointControllerParams bo;
        armarx::navigation::traj_ctrl::toAron(dto, bo);

        return dto.toAron();
    }

    WaypointControllerParams
    WaypointControllerParams::FromAron(const aron::data::DictPtr& dict)
    {
        arondto::WaypointControllerParams dto;
        dto.fromAron(dict);

        WaypointControllerParams bo;
        armarx::navigation::traj_ctrl::fromAron(dto, bo);

        return bo;
    }

    // WaypointController

    WaypointController::WaypointController(const Params& params) :
        params(params)
    {
    }

    TrajectoryControllerResult
    WaypointController::control(const core::Trajectory& goal)
    {
        return {}; // TODO implement
    }
} // namespace armarx::navigation::traj_ctrl
