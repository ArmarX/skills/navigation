/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/core/MultiDimPIDController.h>

#include <armarx/navigation/trajectory_control/TrajectoryController.h>
#include <armarx/navigation/trajectory_control/aron/TrajectoryFollowingControllerParams.aron.generated.h>

namespace armarx::navigation::traj_ctrl
{
    // using arondto::TrajectoryFollowingControllerParams;

    struct TrajectoryFollowingControllerParams : public TrajectoryControllerParams
    {

        core::PIDParams pidPos;
        core::PIDParams pidOri;

        Algorithms algorithm() const override;
        aron::data::DictPtr toAron() const override;

        static TrajectoryFollowingControllerParams FromAron(const aron::data::DictPtr& dict);
    };

    class TrajectoryFollowingController : virtual public TrajectoryController
    {
    public:
        using Params = TrajectoryFollowingControllerParams;

        TrajectoryFollowingController(const VirtualRobot::RobotPtr& robot, const Params& params);
        ~TrajectoryFollowingController() override = default;

        TrajectoryControllerResult control(const core::Trajectory& trajectory) override;

        //    protected:
        core::Twist applyTwistLimits(core::Twist twist);

    private:
        const VirtualRobot::RobotPtr robot;
        const Params params;


        MultiDimPIDControllerTemplate<3> pidPos;
        MultiDimPIDControllerTemplate<3> pidPosTarget;
        MultiDimPIDControllerTemplate<3> pidOri;
        MultiDimPIDControllerTemplate<3> pidOriTarget;
    };
    using TrajectoryFollowingControllerPtr = std::shared_ptr<TrajectoryFollowingController>;

} // namespace armarx::navigation::traj_ctrl
