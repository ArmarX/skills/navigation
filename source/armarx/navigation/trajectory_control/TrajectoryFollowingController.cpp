#include "TrajectoryFollowingController.h"

#include <SimoxUtility/math/convert/mat4f_to_pos.h>
#include <SimoxUtility/math/convert/mat4f_to_rpy.h>
#include <SimoxUtility/math/convert/mat4f_to_xyyaw.h>
#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/interface/serialization/Eigen/Eigen_fdi.h>

#include <RobotAPI/libraries/core/MultiDimPIDController.h>

#include "armarx/navigation/core/basic_types.h"
#include <armarx/navigation/core/Trajectory.h>
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/trajectory_control/TrajectoryController.h>
#include <armarx/navigation/trajectory_control/aron/TrajectoryFollowingControllerParams.aron.generated.h>
#include <armarx/navigation/trajectory_control/aron_conversions.h>
#include <armarx/navigation/trajectory_control/core.h>

namespace armarx::navigation::traj_ctrl
{
    // TrajectoryFollowingControllerParams

    Algorithms
    TrajectoryFollowingControllerParams::algorithm() const
    {
        return Algorithms::TrajectoryFollowingController;
    }

    aron::data::DictPtr
    TrajectoryFollowingControllerParams::toAron() const
    {
        arondto::TrajectoryFollowingControllerParams dto;

        TrajectoryFollowingControllerParams bo;
        armarx::navigation::traj_ctrl::toAron(dto, bo);

        return dto.toAron();
    }

    TrajectoryFollowingControllerParams
    TrajectoryFollowingControllerParams::FromAron(const aron::data::DictPtr& dict)
    {
        arondto::TrajectoryFollowingControllerParams dto;
        dto.fromAron(dict);

        TrajectoryFollowingControllerParams bo;
        armarx::navigation::traj_ctrl::fromAron(dto, bo);

        return bo;
    }

    // TrajectoryFollowingController

    TrajectoryFollowingController::TrajectoryFollowingController(
        const VirtualRobot::RobotPtr& robot,
        const Params& params) :
        robot(robot),
        params(params),
        pidPos(params.pidPos.Kp,
               params.pidPos.Ki,
               params.pidPos.Kd,
               std::numeric_limits<double>::max(),
               std::numeric_limits<double>::max(),
               false,
               std::vector<bool>{false, false, false}),
        pidPosTarget(params.pidPos.Kp / 2,
                     params.pidPos.Ki,
                     params.pidPos.Kd,
                     std::numeric_limits<double>::max(),
                     std::numeric_limits<double>::max(),
                     false,
                     std::vector<bool>{false, false, false}),
        pidOri(params.pidOri.Kp,
               params.pidOri.Ki,
               params.pidOri.Kd,
               std::numeric_limits<double>::max(),
               std::numeric_limits<double>::max(),
               false,
               std::vector<bool>{true, true, true}),
        pidOriTarget(params.pidOri.Kp,
                     params.pidOri.Ki,
                     params.pidOri.Kd,
                     std::numeric_limits<double>::max(),
                     std::numeric_limits<double>::max(),
                     false,
                     std::vector<bool>{true, true, true})
    {
        ARMARX_IMPORTANT << "Trajectory following controller params: "
                         << VAROUT(params.limits.linear) << ", " << VAROUT(params.limits.angular);
    }

    core::Twist
    TrajectoryFollowingController::applyTwistLimits(core::Twist twist)
    {
        const core::Twist limits{.linear = Eigen::Vector3f::Ones() * params.limits.linear,
                                 .angular = Eigen::Vector3f::Ones() * params.limits.angular};

        // for all entries, scale should be less than 1
        const auto scalePos = twist.linear.cwiseAbs().cwiseQuotient(limits.linear.cwiseAbs());
        const auto scaleOri = twist.angular.cwiseAbs().cwiseQuotient(limits.angular.cwiseAbs());

        const float scaleMax = std::max(scalePos.maxCoeff(), scaleOri.maxCoeff());

        if (scaleMax < 1.0F) // both linear and angular velocity in bounds?
        {
            return twist;
        }

        // scale such that no limit is violated
        twist.linear /= scaleMax;
        twist.angular /= scaleMax;

        constexpr float eps = 0.001;

        // pedantic checks
        ARMARX_CHECK_LESS_EQUAL(std::abs(twist.linear.x()), params.limits.linear + eps);
        ARMARX_CHECK_LESS_EQUAL(std::abs(twist.linear.y()), params.limits.linear + eps);
        ARMARX_CHECK_LESS_EQUAL(std::abs(twist.linear.z()), params.limits.linear + eps);
        ARMARX_CHECK_LESS_EQUAL(std::abs(twist.angular.x()), params.limits.angular + eps);
        ARMARX_CHECK_LESS_EQUAL(std::abs(twist.angular.y()), params.limits.angular + eps);
        ARMARX_CHECK_LESS_EQUAL(std::abs(twist.angular.z()), params.limits.angular + eps);

        return twist;
    }

    TrajectoryControllerResult
    TrajectoryFollowingController::control(const core::Trajectory& trajectory)
    {
        ARMARX_CHECK_NOT_NULL(robot) << "Robot not available";

        if (trajectory.points().empty())
        {
            ARMARX_INFO << "Trajectory is empty.";
            return TrajectoryControllerResult{
                .twist = core::Twist::Zero(),
                .dropPoint = {.waypoint = {.pose = core::Pose::Identity()}, .velocity = 0}};
        }

        const core::Pose currentPose(robot->getGlobalPose());

        const auto projectedPose = trajectory.getProjection(
            currentPose.translation(), core::VelocityInterpolation::LinearInterpolation);

        using simox::math::mat4f_to_pos;
        using simox::math::mat4f_to_rpy;

        pidPos.update(mat4f_to_pos(currentPose.matrix()),
                      mat4f_to_pos(projectedPose.projection.waypoint.pose.matrix()));
        pidOri.update(mat4f_to_rpy(currentPose.matrix()),
                      mat4f_to_rpy(projectedPose.projection.waypoint.pose.matrix()));

        const core::Twist twist = [&]() -> core::Twist
        {
            // on the final segment, bahavior differs
            if (projectedPose.segment == core::Projection::Segment::FINAL)
            {

                ARMARX_INFO << deactivateSpam(1) << "final segment";
                // TODO fairly inefficient to do this every time
                pidPos.reset();
                pidOri.reset();

                pidPosTarget.update(currentPose.translation(),
                                    trajectory.points().back().waypoint.pose.translation());

                pidOriTarget.update(
                    mat4f_to_rpy(currentPose.matrix()),
                    mat4f_to_rpy(trajectory.points().back().waypoint.pose.matrix()));

                return core::Twist{.linear = pidPosTarget.getControlValue(),
                                   .angular = pidOriTarget.getControlValue()};
            }

            // pidPosTarget not used yet
            // TODO fairly inefficient to do this every time
            pidPosTarget.reset();

            // the "standard" case following the trajectory
            const Eigen::Vector3f desiredMovementDirection =
                (projectedPose.wayPointAfter.waypoint.pose.translation() -
                 projectedPose.wayPointBefore.waypoint.pose.translation())
                    .normalized();

            const float ffVel = projectedPose.projection.velocity;
            ARMARX_CHECK_FINITE(ffVel);
            ARMARX_CHECK_LESS(ffVel, 3000); // 3 m/s should be sufficient, right?

            const auto feedforwardVelocity = desiredMovementDirection * ffVel;

            ARMARX_VERBOSE << deactivateSpam(1) << "Feed forward direction "
                           << feedforwardVelocity.normalized();
            ARMARX_VERBOSE << deactivateSpam(1) << "Feed forward velocity " << feedforwardVelocity;
            ARMARX_VERBOSE << deactivateSpam(1) << "Control value " << pidPos.getControlValue();

            return core::Twist{.linear = pidPos.getControlValue() + feedforwardVelocity,
                               .angular = pidOri.getControlValue()};
        }();

        const auto twistLimited = applyTwistLimits(twist);
        ARMARX_VERBOSE << deactivateSpam(1) << "Twist limited " << twistLimited.linear.transpose();
        ARMARX_VERBOSE << deactivateSpam(1) << "Twist angular " << twistLimited.angular.transpose();

        // convert to the robot's base frame
        const core::Pose global_T_robot(robot->getGlobalPose());

        const auto& twistGlobal = twistLimited;

        core::Twist twistLocal;
        twistLocal.linear = global_T_robot.linear().inverse() * twistGlobal.linear;
        // TODO if not in 2D, then this must be changed!
        twistLocal.angular = twistGlobal.angular;

        return TrajectoryControllerResult{.twist = twistLocal,
                                          .dropPoint = projectedPose.projection};
    }

} // namespace armarx::navigation::traj_ctrl
