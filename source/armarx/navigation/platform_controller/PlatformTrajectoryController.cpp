#include "PlatformTrajectoryController.h"

#include "ArmarXCore/core/ArmarXObjectScheduler.h"
#include "ArmarXCore/core/logging/Logging.h"
#include "ArmarXCore/core/time/CycleUtil.h"

#include "RobotAPI/components/units/RobotUnit/NJointControllers/NJointControllerRegistry.h"
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetHolonomicPlatformVelocity.h>

#include <armarx/control/common/aron_conversions.h>
#include <armarx/control/common/type.h>
// #include <armarx/control/common/utils.h>
#include <armarx/navigation/common/controller_types.h>
#include <armarx/navigation/platform_controller/aron/PlatformTrajectoryControllerConfig.aron.generated.h>
#include <armarx/navigation/platform_controller/aron_conversions.h>

namespace armarx::navigation::platform_controller::platform_trajectory
{
    const NJointControllerRegistration<Controller> Registration(
        common::ControllerTypeNames.to_name(common::ControllerType::PlatformTrajectory));

    Controller::Controller(const RobotUnitPtr& robotUnit,
                           const NJointControllerConfigPtr& config,
                           const VirtualRobot::RobotPtr&)
    {
        ARMARX_IMPORTANT << "Creating "
                         << common::ControllerTypeNames.to_name(
                                common::ControllerType::PlatformTrajectory);
        // config
        ConfigPtrT cfg = ConfigPtrT::dynamicCast(config);
        ARMARX_CHECK_EXPRESSION(cfg);
        ARMARX_CHECK_EXPRESSION(!cfg->nodeSetName.empty());

        ARMARX_CHECK_EXPRESSION(robotUnit);

        const auto robot = useSynchronizedRtRobot();

        platformTarget = useControlTarget(robotUnit->getRobotPlatformName(),
                                          ControlModes::HolonomicPlatformVelocity)
                             ->asA<ControlTargetHolonomicPlatformVelocity>();
        ARMARX_CHECK_EXPRESSION(platformTarget)
            << "The actuator " << robotUnit->getRobotPlatformName() << " has no control mode "
            << ControlModes::HolonomicPlatformVelocity;

        const auto configData = ::armarx::fromAron<arondto::Config, Config>(cfg->config);
        const auto trajectoryFollowingControllerParams = configData.params;

        reinitTripleBuffer({});
        configBuffer.reinitAllBuffers(configData);

        trajectoryFollowingController.emplace(robot, trajectoryFollowingControllerParams);

        ARMARX_INFO << "Init done.";
    }

    std::string
    Controller::getClassName(const Ice::Current& iceCurrent) const
    {
        return armarx::navigation::common::ControllerTypeNames.to_name(
            armarx::navigation::common::ControllerType::PlatformTrajectory);
    }

    void
    Controller::rtRun(const IceUtil::Time& sensorValuesTimestamp,
                      const IceUtil::Time& timeSinceLastIteration)
    {
        rtUpdateControlStruct();

        platformTarget->velocityX = rtGetControlStruct().platformVelocityTargets.x;
        platformTarget->velocityY = rtGetControlStruct().platformVelocityTargets.y;
        platformTarget->velocityRotation = rtGetControlStruct().platformVelocityTargets.yaw;
    }

    void
    Controller::updateConfig(const ::armarx::aron::data::dto::DictPtr& dto,
                             const Ice::Current& iceCurrent)
    {
        ARMARX_IMPORTANT << "Controller::updateConfig";

        // TODO maybe update pid controller

        auto updateConfig = ::armarx::fromAron<arondto::Config, Config>(dto);
        configBuffer.reinitAllBuffers(updateConfig);

        ARMARX_INFO << "Trajectory with " << updateConfig.targets.trajectory.points().size();

        ARMARX_IMPORTANT << "done Controller::updateConfig";
    }

    void
    Controller::additionalTask()
    {
        ARMARX_CHECK(trajectoryFollowingController.has_value());

        // if trajectory is empty, set velocity to 0
        if (configBuffer.getUpToDateReadBuffer().targets.trajectory.points().empty())
        {
            ARMARX_INFO << deactivateSpam(1) << "Trajectory is empty!";

            getWriterControlStruct().platformVelocityTargets.x = 0;
            getWriterControlStruct().platformVelocityTargets.y = 0;
            getWriterControlStruct().platformVelocityTargets.yaw = 0;
            writeControlStruct();
            return;
        }

        // update controller
        const armarx::navigation::traj_ctrl::TrajectoryControllerResult result =
            trajectoryFollowingController->control(
                configBuffer.getUpToDateReadBuffer().targets.trajectory);

        // store result
        getWriterControlStruct().platformVelocityTargets.x = result.twist.linear.x();
        getWriterControlStruct().platformVelocityTargets.y = result.twist.linear.y();
        getWriterControlStruct().platformVelocityTargets.yaw = result.twist.angular.z();

        writeControlStruct();
    }

    void
    Controller::onPublish(const SensorAndControl& sac,
                          const DebugDrawerInterfacePrx& debugDrawer,
                          const DebugObserverInterfacePrx& debugObservers)
    {
        StringVariantBaseMap datafields;

        datafields["vx"] = new Variant(rtGetControlStruct().platformVelocityTargets.x);
        datafields["vy"] = new Variant(rtGetControlStruct().platformVelocityTargets.y);
        datafields["vyaw"] = new Variant(rtGetControlStruct().platformVelocityTargets.yaw);
        datafields["trajectory_points"] = new Variant(configBuffer.getUpToDateReadBuffer().targets.trajectory.points().size());

        debugObservers->setDebugChannel(
            common::ControllerTypeNames.to_name(common::ControllerType::PlatformTrajectory),
            datafields);
    }

    void
    Controller::onInitNJointController()
    {
        runTask("PlatformTrajectoryControllerAdditionalTask",
                [&]
                {
                    CycleUtil c(10);
                    getObjectScheduler()->waitForObjectStateMinimum(eManagedIceObjectStarted);
                    ARMARX_IMPORTANT << "Create a new thread alone PlatformTrajectory controller";
                    while (getState() == eManagedIceObjectStarted)
                    {
                        if (isControllerActive() and rtReady.load())
                        {
                            ARMARX_VERBOSE << "additional task";
                            additionalTask();
                        }
                        c.waitForCycleDuration();
                    }
                });

        ARMARX_INFO << "PlatformTrajectoryVelocityController::onInitNJointController";
    }

    void
    Controller::rtPreActivateController()
    {
        rtReady.store(true);
    }


    Controller::~Controller() = default;

} // namespace armarx::navigation::platform_controller::platform_trajectory
