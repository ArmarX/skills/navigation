/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/VirtualRobot.h>

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointControllerWithTripleBuffer.h>
#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueHolonomicPlatform.h>

#include <armarx/control/interface/ConfigurableNJointControllerInterface.h>
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/trajectory_control/TrajectoryFollowingController.h>

namespace armarx
{
    class ControlTargetHolonomicPlatformVelocity;
    class SensorValueHolonomicPlatformWithAbsolutePosition;
} // namespace armarx


namespace armarx::navigation::platform_controller::platform_trajectory
{
    struct Targets
    {
        core::Trajectory trajectory;
    };

    struct Config
    {
        using Params = traj_ctrl::TrajectoryFollowingControllerParams;

        Params params;
        Targets targets;
    };

    struct Target
    {
        struct
        {
            double x = 0;
            double y = 0;
            double yaw = 0;
        } platformVelocityTargets;

        void
        reset()
        {
            platformVelocityTargets.x = 0;
            platformVelocityTargets.y = 0;
            platformVelocityTargets.yaw = 0;
        }
    };

    struct Devices
    {
        ControlTargetHolonomicPlatformVelocity* platformTarget;
    };

    using NameValueMap = std::map<std::string, float>;

    class Controller :
        virtual public NJointControllerWithTripleBuffer<Target>,
        virtual public armarx::control::ConfigurableNJointControllerInterface
    {
    public:
        using ConfigPtrT = control::ConfigurableNJointControllerConfigPtr;

        Controller(const RobotUnitPtr& robotUnit,
                   const NJointControllerConfigPtr& config,
                   const VirtualRobot::RobotPtr&);

        ~Controller() override;

        std::string getClassName(const Ice::Current& iceCurrent = Ice::emptyCurrent) const override;

        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;

        void updateConfig(const ::armarx::aron::data::dto::DictPtr& dto,
                          const Ice::Current& iceCurrent = Ice::emptyCurrent) override;

    protected:
        void additionalTask();
        void onPublish(const SensorAndControl& sac,
                       const DebugDrawerInterfacePrx& debugDrawer,
                       const DebugObserverInterfacePrx& debugObservers) override;


        void onInitNJointController() override;
        void rtPreActivateController() override;

    private:
        // [[nodiscard]] bool initializeQPIK();

        // void updateDebugStatus();

        // struct DebugStatus
        // {
        //     std::map<std::string, float> data;
        // };

        TripleBuffer<Config> configBuffer;

        // internal
        std::atomic_bool rtFirstRun = true;
        std::atomic_bool rtReady = false;

        ControlTargetHolonomicPlatformVelocity* platformTarget;

        std::optional<traj_ctrl::TrajectoryFollowingController> trajectoryFollowingController;


        Devices getDevices(const VirtualRobot::RobotNodeSet& rns);

        // NameValueMap getJointVelocities(const NameValueMap& initialJointValues,
        //                                 const NameValueMap& targetJointValues,
        //                                 const std::set<std::string>& joints);

        // Eigen::VectorXd toOptikRobotState(const NameValueMap& jointValues) const;

        // NameValueMap getJointValuesWithVirtualPlatformJoints();

        // NameValueMap getPlatformJointVelocity(const NameValueMap& initialJointAngles,
        //                                       const NameValueMap& targetJointAngles) const;
    };

}  // namespace armarx::navigation::platform_controller::platform_trajectory
