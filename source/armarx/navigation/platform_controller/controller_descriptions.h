/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <armarx/control/client/ControllerDescription.h>
#include <armarx/navigation/common/controller_types.h>
#include <armarx/navigation/platform_controller/PlatformTrajectoryController.h>
#include <armarx/navigation/platform_controller/aron/PlatformTrajectoryControllerConfig.aron.generated.h>

namespace armarx::control::client
{
    template <>
    struct ControllerDescription<armarx::navigation::common::ControllerType::PlatformTrajectory>
    {
        using AronDTO =
            armarx::navigation::platform_controller::platform_trajectory::arondto::Config;

        using BO = armarx::navigation::platform_controller::platform_trajectory::Config;

        // constexpr static const char* name = armarx::navigation::common::ControllerTypeNames.to_name(armarx::navigation::common::ControllerType::PlatformTrajectory);
        constexpr static const char* name =
            armarx::navigation::common::PlatformTrajectoryControllerName;
    };

} // namespace armarx::control::client
