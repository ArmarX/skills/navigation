/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::client
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <armarx/navigation/core/types.h>
#include <armarx/navigation/factories/NavigationStackFactory.h>
#include <armarx/navigation/global_planning/AStar.h>
#include <armarx/navigation/server/NavigationStack.h>
#include <armarx/navigation/server/Navigator.h>

#define BOOST_TEST_MODULE Navigation::ArmarXLibraries::client

#define ARMARX_BOOST_TEST

#include <iostream>

#include "../NavigationStackConfig.h"
#include <armarx/navigation/Test.h>

using namespace armarx::navigation;

BOOST_AUTO_TEST_CASE(testExample)
{

    // create navigation stack config
    client::NavigationStackConfig cfg;
    cfg.configureGlobalPlanner(global_planning::AStarParams());

    const auto stackConfig = cfg.toAron();
    // here, we would send data over Ice ...

    core::Pose goal = core::Pose::Identity();
    Eigen::Vector3f direction;

    // NavigatorPrx navigator;
    // navigator->moveTo(goal, stackConfig, core::NavigationFrame::Absolute);
    // navigator->moveTorwards(direction, stackConfig, core::NavigationFrame::Relative);

    // CLIENT
    ////////////////////////////////////////////////
    // SERVER

    // create Navigator
    core::Context ctx;
    server::NavigationStack stack = fac::NavigationStackFactory::create(stackConfig, ctx);

    server::Navigator navigator(stack, ctx);
    // DirectionNavigator
    // RelativePointNavigator

    navigator.moveTo(goal, core::NavigationFrames::Absolute);

    BOOST_CHECK_EQUAL(true, true);
}
