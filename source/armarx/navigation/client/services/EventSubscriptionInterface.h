#pragma once


// STD/STL
#include <functional>

// Navigation
#include <armarx/navigation/core/events.h>
#include <armarx/navigation/global_planning/GlobalPlanner.h>
#include <armarx/navigation/local_planning/LocalPlanner.h>
#include <armarx/navigation/trajectory_control/TrajectoryController.h>


namespace armarx::navigation::client
{

    using GlobalTrajectoryUpdatedCallback =
        std::function<void(const global_planning::GlobalPlannerResult&)>;
    using LocalTrajectoryUpdatedCallback = std::function<void(const loc_plan::LocalPlannerResult&)>;
    using TrajectoryControllerUpdatedCallback =
        std::function<void(const traj_ctrl::TrajectoryControllerResult&)>;
    using GlobalPlanningFailedCallback =
        std::function<void(const core::GlobalPlanningFailedEvent&)>;

    using OnMovementStartedCallback = std::function<void(const core::MovementStartedEvent&)>;

    using OnGoalReachedCallback = std::function<void(const core::GoalReachedEvent&)>;
    using OnWaypointReachedCallback = std::function<void(const core::WaypointReachedEvent&)>;
    using OnSafetyThrottlingTriggeredCallback =
        std::function<void(const core::SafetyThrottlingTriggeredEvent&)>;
    using OnSafetyStopTriggeredCallback =
        std::function<void(const core::SafetyStopTriggeredEvent&)>;
    using OnUserAbortTriggeredCallback = std::function<void(const core::UserAbortTriggeredEvent&)>;
    using OnInternalErrorCallback = std::function<void(const core::InternalErrorEvent&)>;

    class EventSubscriptionInterface
    {

    public:
        virtual void onMovementStarted(const OnMovementStartedCallback& callback) = 0;
        virtual void onGoalReached(const OnGoalReachedCallback& callback) = 0;

        virtual void onWaypointReached(const OnWaypointReachedCallback& callback) = 0;

        virtual void
        onSafetyThrottlingTriggered(const OnSafetyThrottlingTriggeredCallback& callback) = 0;

        virtual void onSafetyStopTriggered(const OnSafetyStopTriggeredCallback& callback) = 0;

        virtual void onUserAbortTriggered(const OnUserAbortTriggeredCallback& callback) = 0;

        virtual void onInternalError(const OnInternalErrorCallback& callback) = 0;

        virtual void onGlobalPlanningFailed(const GlobalPlanningFailedCallback& callback) = 0;

        // Non-API.
    public:
        virtual ~EventSubscriptionInterface() = default;
    };

} // namespace armarx::navigation::client
