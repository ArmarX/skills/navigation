#pragma once


#include <armarx/navigation/client/services/EventSubscriptionInterface.h>
#include <armarx/navigation/core/events.h>
#include <armarx/navigation/server/event_publishing/EventPublishingInterface.h>


namespace armarx::navigation::client
{

    class SimpleEventHandler :
        virtual public EventSubscriptionInterface,
        virtual public server::EventPublishingInterface
    {

        // EventSubscriptionInterface
    public:
        void onGoalReached(const OnGoalReachedCallback& callback) override;
        void onWaypointReached(const OnWaypointReachedCallback& callback) override;
        void
        onSafetyThrottlingTriggered(const OnSafetyThrottlingTriggeredCallback& callback) override;
        void onSafetyStopTriggered(const OnSafetyStopTriggeredCallback& callback) override;
        void onUserAbortTriggered(const OnUserAbortTriggeredCallback& callback) override;
        void onInternalError(const OnInternalErrorCallback& callback) override;

        void onMovementStarted(const OnMovementStartedCallback& callback) override;
        void onGlobalPlanningFailed(const GlobalPlanningFailedCallback& callback) override;

        // EventPublishingInterface
    public:
        void globalTrajectoryUpdated(const global_planning::GlobalPlannerResult& event) override;
        void localTrajectoryUpdated(const loc_plan::LocalPlannerResult& event) override;
        void
        trajectoryControllerUpdated(const traj_ctrl::TrajectoryControllerResult& event) override;
        void globalPlanningFailed(const core::GlobalPlanningFailedEvent& event) override;

        void movementStarted(const core::MovementStartedEvent& event) override;
        void goalReached(const core::GoalReachedEvent& event) override;
        void waypointReached(const core::WaypointReachedEvent& event) override;
        void safetyThrottlingTriggered(const core::SafetyThrottlingTriggeredEvent& event) override;
        void safetyStopTriggered(const core::SafetyStopTriggeredEvent& event) override;
        void userAbortTriggered(const core::UserAbortTriggeredEvent& event) override;
        void internalError(const core::InternalErrorEvent& event) override;

    private:
        struct
        {
            std::vector<GlobalTrajectoryUpdatedCallback> globalTrajectoryUpdatedCallbacks;
            std::vector<LocalTrajectoryUpdatedCallback> localTrajectoryUpdatedCallbacks;
            std::vector<TrajectoryControllerUpdatedCallback> trajectoryControllerUpdatedCallbacks;
            std::vector<GlobalPlanningFailedCallback> globalPlanningFailedCallbacks;

            std::vector<OnMovementStartedCallback> movementStartedCallbacks;
            std::vector<OnGoalReachedCallback> goalReachedCallbacks;
            std::vector<OnWaypointReachedCallback> waypointReachedCallbacks;
            std::vector<OnSafetyThrottlingTriggeredCallback> safetyThrottlingTriggeredCallbacks;
            std::vector<OnSafetyStopTriggeredCallback> safetyStopTriggeredCallbacks;
            std::vector<OnUserAbortTriggeredCallback> userAbortTriggeredCallbacks;
            std::vector<OnInternalErrorCallback> internalErrorCallbacks;
        } subscriptions;
    };

} // namespace armarx::navigation::client
