#include <armarx/navigation/client/services/SimpleEventHandler.h>


void
armarx::navigation::client::SimpleEventHandler::onGoalReached(const OnGoalReachedCallback& callback)
{
    subscriptions.goalReachedCallbacks.push_back(callback);
}


void
armarx::navigation::client::SimpleEventHandler::onWaypointReached(
    const OnWaypointReachedCallback& callback)
{
    subscriptions.waypointReachedCallbacks.push_back(callback);
}


void
armarx::navigation::client::SimpleEventHandler::onSafetyThrottlingTriggered(
    const OnSafetyThrottlingTriggeredCallback& callback)
{
    subscriptions.safetyThrottlingTriggeredCallbacks.push_back(callback);
}


void
armarx::navigation::client::SimpleEventHandler::onSafetyStopTriggered(
    const OnSafetyStopTriggeredCallback& callback)
{
    subscriptions.safetyStopTriggeredCallbacks.push_back(callback);
}


void
armarx::navigation::client::SimpleEventHandler::onUserAbortTriggered(
    const OnUserAbortTriggeredCallback& callback)
{
    subscriptions.userAbortTriggeredCallbacks.push_back(callback);
}


void
armarx::navigation::client::SimpleEventHandler::onInternalError(
    const OnInternalErrorCallback& callback)
{
    subscriptions.internalErrorCallbacks.push_back(callback);
}

void
armarx::navigation::client::SimpleEventHandler::SimpleEventHandler::onMovementStarted(
    const OnMovementStartedCallback& callback)
{
    subscriptions.movementStartedCallbacks.push_back(callback);
}

void
armarx::navigation::client::SimpleEventHandler::onGlobalPlanningFailed(const GlobalPlanningFailedCallback& callback)
{
    subscriptions.globalPlanningFailedCallbacks.push_back(callback);
}

void
armarx::navigation::client::SimpleEventHandler::goalReached(const core::GoalReachedEvent& event)
{
    for (const auto& callback : subscriptions.goalReachedCallbacks)
    {
        callback(event);
    }
}


void
armarx::navigation::client::SimpleEventHandler::waypointReached(
    const core::WaypointReachedEvent& event)
{
    for (const auto& callback : subscriptions.waypointReachedCallbacks)
    {
        callback(event);
    }
}


void
armarx::navigation::client::SimpleEventHandler::safetyThrottlingTriggered(
    const core::SafetyThrottlingTriggeredEvent& event)
{
    for (const auto& callback : subscriptions.safetyThrottlingTriggeredCallbacks)
    {
        callback(event);
    }
}


void
armarx::navigation::client::SimpleEventHandler::safetyStopTriggered(
    const core::SafetyStopTriggeredEvent& event)
{
    for (const auto& callback : subscriptions.safetyStopTriggeredCallbacks)
    {
        callback(event);
    }
}


void
armarx::navigation::client::SimpleEventHandler::userAbortTriggered(
    const core::UserAbortTriggeredEvent& event)
{
    for (const auto& callback : subscriptions.userAbortTriggeredCallbacks)
    {
        callback(event);
    }
}


void
armarx::navigation::client::SimpleEventHandler::internalError(const core::InternalErrorEvent& event)
{
    for (const auto& callback : subscriptions.internalErrorCallbacks)
    {
        callback(event);
    }
}

namespace armarx::navigation::client
{
    void
    SimpleEventHandler::globalTrajectoryUpdated(const global_planning::GlobalPlannerResult& event)
    {
        for (const auto& callback : subscriptions.globalTrajectoryUpdatedCallbacks)
        {
            callback(event);
        }
    }

    void
    SimpleEventHandler::localTrajectoryUpdated(const loc_plan::LocalPlannerResult& event)
    {
        for (const auto& callback : subscriptions.localTrajectoryUpdatedCallbacks)
        {
            callback(event);
        }
    }

    void
    SimpleEventHandler::trajectoryControllerUpdated(
        const traj_ctrl::TrajectoryControllerResult& event)
    {
        for (const auto& callback : subscriptions.trajectoryControllerUpdatedCallbacks)
        {
            callback(event);
        }
    }

    void
    SimpleEventHandler::globalPlanningFailed(const core::GlobalPlanningFailedEvent& event)
    {
        for (const auto& callback : subscriptions.globalPlanningFailedCallbacks)
        {
            callback(event);
        }
    }

    void
    SimpleEventHandler::movementStarted(const core::MovementStartedEvent& event)
    {
        for (const auto& callback : subscriptions.movementStartedCallbacks)
        {
            callback(event);
        }
    }
} // namespace armarx::navigation::client
