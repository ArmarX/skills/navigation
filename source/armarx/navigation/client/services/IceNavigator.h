#pragma once


#include <string>

#include <armarx/navigation/client/NavigationStackConfig.h>
#include <armarx/navigation/client/ice/NavigatorInterface.h>
#include <armarx/navigation/client/ice_conversions.h>
#include <armarx/navigation/core/NavigatorInterface.h>


namespace armarx::navigation::client
{

    class IceNavigator : virtual public core::NavigatorInterface
    {

    public:
        IceNavigator();
        IceNavigator(const NavigatorInterfacePrx& navigator);

        ~IceNavigator() override;


        void setNavigatorComponent(const NavigatorInterfacePrx& navigator);

        void createConfig(const client::NavigationStackConfig& config, const std::string& configId);

        void moveTo(const std::vector<core::Pose>& waypoints,
                    core::NavigationFrame navigationFrame) override;

        void moveTo(const std::vector<client::WaypointTarget>& targets,
                    core::NavigationFrame navigationFrame) override;

        void moveTowards(const core::Direction& direction,
                         core::NavigationFrame navigationFrame) override;

        void pause() override;

        void resume() override;

        void stop() override;

        bool isPaused() const noexcept override;

        bool isStopped() const noexcept override;

    private:
        std::string configId;

        NavigatorInterfacePrx navigator;
    };

} // namespace armarx::navigation::client
