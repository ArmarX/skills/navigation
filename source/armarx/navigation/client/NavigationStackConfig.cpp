#include "NavigationStackConfig.h"

#include <memory>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/interface/aron/Aron.h>
#include <RobotAPI/libraries/aron/core/data/variant/Factory.h>
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/aron/core/data/variant/primitive/String.h>

#include <armarx/navigation/core/constants.h>
#include <armarx/navigation/core/types.h>


namespace armarx::navigation::client
{
    aron::data::DictPtr
    GeneralConfig::toAron() const
    {
        aron::data::DictPtr element(new aron::data::Dict);
        return element;
    }

    aron::data::dto::DictPtr
    NavigationStackConfig::toAron() const
    {
        ARMARX_CHECK(isValid())
            << "The NavigationStackConfig is not valid as some elements are not set!";
        return dict.toAronDictDTO();
    }

    bool
    NavigationStackConfig::isValid() const
    {
        // TODO what about general config?

        if (not dict.hasElement(core::StackLayerNames.to_name(core::StackLayer::GlobalPlanner)))
        {
            ARMARX_WARNING << "Global planner not set!";
            return false;
        }

        if (not dict.hasElement(
                core::StackLayerNames.to_name(core::StackLayer::TrajectoryController)))
        {
            ARMARX_WARNING << "Trajectory controller not set!";
            return false;
        }

        return true;
    }

    NavigationStackConfig&
    NavigationStackConfig::general(const GeneralConfig& cfg)
    {
        // TODO
        return *this;
    }

    NavigationStackConfig&
    NavigationStackConfig::globalPlanner(const global_planning::GlobalPlannerParams& params)
    {
        aron::data::DictPtr element(new aron::data::Dict);
        element->addElement(core::NAME_KEY,
                            std::make_shared<aron::data::String>(
                                global_planning::AlgorithmNames.to_name(params.algorithm())));
        element->addElement(core::PARAMS_KEY, params.toAron());

        dict.addElement(core::StackLayerNames.to_name(core::StackLayer::GlobalPlanner), element);

        return *this;
    }

    NavigationStackConfig&
    NavigationStackConfig::localPlanner(const loc_plan::LocalPlannerParams& params)
    {
        aron::data::DictPtr element(new aron::data::Dict);
        element->addElement(core::NAME_KEY,
                            std::make_shared<aron::data::String>(
                                loc_plan::AlgorithmNames.to_name(params.algorithm())));
        element->addElement(core::PARAMS_KEY, params.toAron());

        dict.addElement(core::StackLayerNames.to_name(core::StackLayer::LocalPlanner), element);

        return *this;
    }

    NavigationStackConfig&
    NavigationStackConfig::trajectoryController(const traj_ctrl::TrajectoryControllerParams& params)
    {
        aron::data::DictPtr element(new aron::data::Dict);
        element->addElement(core::NAME_KEY,
                            std::make_shared<aron::data::String>(
                                traj_ctrl::AlgorithmNames.to_name(params.algorithm())));
        element->addElement(core::PARAMS_KEY, params.toAron());

        dict.addElement(core::StackLayerNames.to_name(core::StackLayer::TrajectoryController),
                        element);

        return *this;
    }

    NavigationStackConfig&
    NavigationStackConfig::safetyController(const safe_ctrl::SafetyControllerParams& params)
    {
        aron::data::DictPtr element(new aron::data::Dict);
        element->addElement(core::NAME_KEY,
                            std::make_shared<aron::data::String>(
                                safe_ctrl::AlgorithmNames.to_name(params.algorithm())));
        element->addElement(core::PARAMS_KEY, params.toAron());

        dict.addElement(core::StackLayerNames.to_name(core::StackLayer::SafetyController), element);

        return *this;
    }
} // namespace armarx::navigation::client
