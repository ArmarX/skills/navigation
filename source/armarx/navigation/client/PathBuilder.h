/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <limits>

#include <armarx/navigation/client/ice/NavigatorInterface.h>
#include <armarx/navigation/client/ice_conversions.h>
#include <armarx/navigation/client/types.h>
#include <armarx/navigation/core/types.h>


namespace armarx::navigation::client
{

    struct Config
    {
        float maxVel = std::numeric_limits<float>::max();
    };


    class PathBuilder
    {
    public:
        PathBuilder() = default;

        PathBuilder& add(const std::string& locationId,
                         const GlobalPlanningStrategy& strategy = GlobalPlanningStrategy::Free,
                         const Config& config = Config());

        PathBuilder& add(const core::Pose& pose,
                         const GlobalPlanningStrategy& strategy = GlobalPlanningStrategy::Free,
                         const Config& config = Config());

        // PathBuilder& add(...);

        const WaypointTargets& path() const;

    protected:
    private:
        WaypointTargets waypoints;
    };
} // namespace armarx::navigation::client
