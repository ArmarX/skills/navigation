#include <memory>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include <RobotAPI/components/armem/MemoryNameSystem/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/plugins/ListeningPluginUser.h>
#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>

#include <armarx/navigation/client/ComponentPlugin.h>
#include <armarx/navigation/client/Navigator.h>
#include <armarx/navigation/client/services/MemorySubscriber.h>
#include <armarx/navigation/client/services/SimpleEventHandler.h>

// ComponentPlugin

armarx::navigation::client::ComponentPlugin::~ComponentPlugin() = default;

void
armarx::navigation::client::ComponentPlugin::postCreatePropertyDefinitions(
    armarx::PropertyDefinitionsPtr& properties)
{
    if (not properties->hasDefinition(PROPERTY_NAME))
    {
        properties->defineOptionalProperty<std::string>(
            PROPERTY_NAME, "navigator", "Name of the Navigator");
    }
}

void
armarx::navigation::client::ComponentPlugin::preOnInitComponent()
{
    ARMARX_TRACE;
    parent<armarx::Component>().usingProxyFromProperty(PROPERTY_NAME);
}

void
armarx::navigation::client::ComponentPlugin::preOnConnectComponent()
{
    ARMARX_TRACE;
    parent<armarx::Component>().getProxyFromProperty(navigatorPrx, PROPERTY_NAME);

    ARMARX_TRACE;
    const std::string componentName = parent().getName();

    ARMARX_CHECK_NOT_NULL(navigatorPrx) << "Navigator proxy is null!";
    iceNavigator.setNavigatorComponent(navigatorPrx);
}

void
armarx::navigation::client::ComponentPlugin::configureNavigator(
    const client::NavigationStackConfig& stackConfig,
    const std::string& configId)
{
    ARMARX_TRACE;

    // ARMARX_CHECK_NULL(eventHandler) << "`configureNavigator()` can only be called once!";

    eventHandler = [&]() -> std::unique_ptr<SimpleEventHandler>
    {
        if (parentDerives<armarx::armem::client::plugins::ListeningPluginUser>())
        {
            ARMARX_INFO << "Using memory event callbacks.";
            // must(!!) use a reference here: otherwise the mns events won't work
            auto& memoryNameSystem =
                parent<armarx::armem::client::plugins::ListeningPluginUser>().memoryNameSystem();
            return std::make_unique<MemorySubscriber>(configId, memoryNameSystem);
        }

        ARMARX_INFO << "Cannot use memory event callbacks as this component is not derived from "
                       "`armarx::armem::client::plugins::ListeningPluginUser`";

        return std::make_unique<SimpleEventHandler>();
    }();

    iceNavigator.createConfig(stackConfig, configId);

    navigator = std::make_unique<Navigator>(
        Navigator::InjectedServices{.navigator = &iceNavigator, .subscriber = eventHandler.get()});
}

// ComponentPluginUser

armarx::navigation::client::ComponentPluginUser::ComponentPluginUser()
{
    ARMARX_TRACE;
    addPlugin(plugin);
}

void
armarx::navigation::client::ComponentPluginUser::configureNavigator(
    const client::NavigationStackConfig& stackConfig)
{
    ARMARX_TRACE;
    plugin->configureNavigator(stackConfig, getName());
}

armarx::navigation::client::Navigator&
armarx::navigation::client::ComponentPluginUser::getNavigator()
{
    ARMARX_CHECK_NOT_NULL(plugin->navigator)
        << "You need to call `configureNavigator()` before you can access the navigator!";
    return *plugin->navigator;
}

armarx::navigation::client::ComponentPluginUser::~ComponentPluginUser() = default;
