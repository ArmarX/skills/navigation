/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::ExampleClient
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <ArmarXCore/util/tasks.h>

#include <armarx/navigation/client.h>
#include <RobotAPI/libraries/armem/client/plugins/ReaderWriterPlugin.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h>
#include <RobotAPI/libraries/armem/client/plugins.h>


namespace armarx::navigation::components::example_client
{

    /**
     * @defgroup Component-ExampleClient ExampleClient
     * @ingroup armarx_navigation-Components
     * A description of the component ExampleClient.
     *
     * @class ExampleClient
     * @ingroup Component-ExampleClient
     * @brief Brief description of class ExampleClient.
     *
     * Detailed description of class ExampleClient.
     */
    class Component :
        virtual public armarx::Component,
        virtual public armarx::navigation::client::ComponentPluginUser,
        virtual public armem::ListeningClientPluginUser
    {

    public:
        Component();

        ~Component() override;

    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        std::string getDefaultName() const override;

        void exampleNavigation();
        void exampleNavigationComplex();

        /**
         * @brief demonstrates point to point movement.
         * 
         * The robot will move along a rectange in the following way:
         * 
         * (1) move to the right 
         * (2) move forward and turn 90° to the left 
         * (3) move left and turn back 90° to the right 
         * (4) backward (this is the initial pose)
         * 
         */
        void exampleNavigationPointToPoint();

    private:
        void goalReached();

        struct{
            std::string robotName = "Armar6";
            float relativeMovement = 200; // [mm]
        } properties;

        armarx::RunningTask<Component>::pointer_type task;

        armem::client::plugins::ReaderWriterPlugin<armem::robot_state::VirtualRobotReader>* virtualRobotReaderPlugin = nullptr;


    };

} // namespace armarx::navigation::components::example_client
