/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::ExampleClient
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <chrono>
#include <cmath>
#include <thread>

#include <Eigen/src/Geometry/AngleAxis.h>
#include <Eigen/src/Geometry/Translation.h>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/time/Clock.h>
#include <ArmarXCore/core/time/ClockType.h>
#include <ArmarXCore/core/time/forward_declarations.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <armarx/navigation/client/types.h>
#include <armarx/navigation/global_planning/Point2Point.h>
#include <armarx/navigation/client/PathBuilder.h>
#include <armarx/navigation/global_planning/AStar.h>
#include <armarx/navigation/trajectory_control/TrajectoryFollowingController.h>


namespace armarx::navigation::components::example_client
{

    Component::Component()
    {
        addPlugin(virtualRobotReaderPlugin);
    }


    Component::~Component()
    {
        // pass
    }


    void
    Component::onInitComponent()
    {
        // pass
    }


    void
    Component::onConnectComponent()
    {
        task = new armarx::RunningTask<Component>(this, &Component::exampleNavigationPointToPoint);
        task->start();
    }


    void
    Component::onDisconnectComponent()
    {
        const bool join = true;
        task->stop(join);
    }


    void
    Component::onExitComponent()
    {
        // pass
    }


    std::string
    Component::getDefaultName() const
    {
        return "ExampleClient";
    }


    void
    Component::exampleNavigation()
    {
        // Import relevant namespaces.
        using namespace armarx::navigation;

        ARMARX_INFO << "Configuring navigator";

        // Create an example configuration valid for the following move* calls.
        configureNavigator(
            client::NavigationStackConfig()
                .general({} /*{.maxVel = VelocityLimits{.linear = 400 , .angular = 0.1}}*/)
                .globalPlanner(global_planning::AStarParams())
                .trajectoryController(traj_ctrl::TrajectoryFollowingControllerParams()));

        // Example of registering a lambda as callback.
        getNavigator().onGoalReached([&]() { ARMARX_IMPORTANT << "Goal reached! (lambda-style)"; });

        // Example of registering a method as callback.
        getNavigator().onGoalReached([this] { goalReached(); });

        Clock::WaitFor(Duration::Seconds(1));

        ARMARX_INFO << "Moving to goal pose";
        // Start moving to goal position using above config.
        core::Pose goal = core::Pose::Identity();
        goal.translation() << 2000, 1000, 0;

        getNavigator().moveTo(goal, core::NavigationFrame::Absolute);

        // Wait until goal is reached
        armarx::navigation::client::StopEvent se = getNavigator().waitForStop();
        if (se)
        {
            ARMARX_INFO << "Goal 1 reached.";
        }
        else
        {
            if (se.isSafetyStopTriggeredEvent())
            {
                ARMARX_ERROR << "Safety stop was triggered!";
            }
            else if (se.isUserAbortTriggeredEvent())
            {
                ARMARX_ERROR << "Aborted by user!";
            }
            else if (se.isInternalErrorEvent())
            {
                ARMARX_ERROR << "Unknown internal error occured! "
                             << se.toInternalErrorEvent().message;
            }
        }

        goal.translation() << -1500, 1000, 0;
        getNavigator().moveTo(goal, core::NavigationFrame::Absolute);

        Clock::WaitFor(Duration::Seconds(15));

        // Wait until goal is reached
        se = getNavigator().waitForStop();
        if (se)
        {
            ARMARX_INFO << "Goal 2 reached.";
        }
        else
        {
            ARMARX_ERROR << "Could not reach goal 2!";
        }

        goal.translation() << 4500, 4500, 0;
        getNavigator().moveTo(goal, core::NavigationFrame::Absolute);

        // Wait until goal is reached
        se = getNavigator().waitForStop();
        if (se)
        {
            ARMARX_INFO << "Goal 3 reached.";
        }
        else
        {
            ARMARX_ERROR << "Could not reach goal 3!";
        }

        Clock::WaitFor(Duration::Seconds(15));

        // TODO example with waypoints

        ARMARX_INFO << "Moving into certain direction.";
        // Start moving towards a direction
        getNavigator().moveTowards(core::Direction(100, 100, 0), core::NavigationFrame::Relative);

        Clock::WaitFor(Duration::Seconds(3));

        ARMARX_INFO << "Pausing movement.";
        getNavigator().pause();
    }

    void
    Component::exampleNavigationComplex()
    {
        ARMARX_INFO << "Demonstrating complex navigation";

        // Import relevant namespaces.
        using namespace armarx::navigation;

        ARMARX_INFO << "Configuring navigator";

        // Create an example configuration valid for the following move* calls.
        configureNavigator(
            client::NavigationStackConfig()
                .general({} /*{.maxVel = VelocityLimits{.linear = 400 , .angular = 0.1}}*/)
                .globalPlanner(global_planning::AStarParams())
                .trajectoryController(traj_ctrl::TrajectoryFollowingControllerParams()));

        // Example of registering a lambda as callback.
        getNavigator().onGoalReached([&]() { ARMARX_IMPORTANT << "Goal reached! (lambda-style)"; });

        // Example of registering a method as callback.
        getNavigator().onGoalReached([this] { goalReached(); });

        Clock::WaitFor(Duration::Seconds(1));

        ARMARX_INFO << "Moving to goal pose";
        // Start moving to goal position using above config.

        core::Pose anonymousWaypoint = core::Pose::Identity();
        anonymousWaypoint.translation() << 2000, 1000, 0;

        core::Pose goal = core::Pose::Identity();
        goal.translation() << 2000, 1500, 0;

        client::PathBuilder pathBuilder;

        // clang-format off
        pathBuilder
            .add(anonymousWaypoint)
            .add("Testing/home")
            .add(goal);
        // clang-format on

        ARMARX_INFO << "Starting execution";
        getNavigator().moveTo(pathBuilder, core::NavigationFrame::Absolute);

        // Wait until goal is reached
        armarx::navigation::client::StopEvent se = getNavigator().waitForStop();
        if (se)
        {
            ARMARX_INFO << "Goal 1 reached.";
        }
        else
        {
            if (se.isSafetyStopTriggeredEvent())
            {
                ARMARX_ERROR << "Safety stop was triggered!";
            }
            else if (se.isUserAbortTriggeredEvent())
            {
                ARMARX_ERROR << "Aborted by user!";
            }
            else if (se.isInternalErrorEvent())
            {
                ARMARX_ERROR << "Unknown internal error occured! "
                             << se.toInternalErrorEvent().message;
            }
        }

        ARMARX_INFO << "Pausing movement.";
        getNavigator().pause();
    }


    void
    Component::exampleNavigationPointToPoint()
    {
        ARMARX_INFO << "Demonstrating point to point navigation";

        using namespace std::chrono_literals;

        // Import relevant namespaces.
        using namespace armarx::navigation;

        ARMARX_INFO << "Configuring navigator";

        // Create an example configuration valid for the following move* calls.
        configureNavigator(
            client::NavigationStackConfig()
                .general({} /*{.maxVel = VelocityLimits{.linear = 400 , .angular = 0.1}}*/)
                .globalPlanner(global_planning::Point2PointParams())
                .trajectoryController(traj_ctrl::TrajectoryFollowingControllerParams()));

        Clock::WaitFor(Duration::Seconds(1));

        ARMARX_INFO << "Moving to goal pose";
        // Start moving to goal position using above config.

        auto& virtualRobotReader = virtualRobotReaderPlugin->get();

        auto robot = virtualRobotReader.getSynchronizedRobot(properties.robotName);
        ARMARX_CHECK_NOT_NULL(robot);

        const core::Pose initialPose(robot->getGlobalPose());

        const core::Pose goal1 =
            initialPose * Eigen::Translation3f(properties.relativeMovement, 0, 0); // to the right

        const core::Pose goal2 =
            goal1 * Eigen::Translation3f(0, properties.relativeMovement, 0) *
            Eigen::AngleAxisf(M_PI_2f32, Eigen::Vector3f::UnitZ()); // forward + 90° to the left

        const core::Pose goal3 =
            goal2 * Eigen::Translation3f(-properties.relativeMovement, 0, 0) *
            Eigen::AngleAxisf(-M_PI_2f32,
                              Eigen::Vector3f::UnitZ()); // left and back to initial orientation

        const core::Pose& finalGoal = initialPose;

        const auto moveToP2P = [&](const core::Pose& targetPose)
        {
            // define movement
            client::PathBuilder pathBuilder;
            pathBuilder.add(targetPose,
                            armarx::navigation::client::GlobalPlanningStrategy::Point2Point);

            // trigger movement
            ARMARX_INFO << "Starting execution";
            getNavigator().moveTo(pathBuilder, core::NavigationFrame::Absolute);

            // wait for navigator to reach the target
            ARMARX_INFO << "Waiting for navigator to reach the target";
            armarx::navigation::client::StopEvent se = getNavigator().waitForStop();

            // In case of any failure, the following will raise an exception to avoid subsequent navigation requests to be executed.
            ARMARX_CHECK(se.isGoalReachedEvent());
        };

        ARMARX_IMPORTANT << "Moving to goal 1";
        moveToP2P(goal1);

        ARMARX_IMPORTANT << "Moving to goal 2";
        moveToP2P(goal2);

        ARMARX_IMPORTANT << "Moving to goal 3";
        moveToP2P(goal3);

        ARMARX_IMPORTANT << "Moving to final goal";
        moveToP2P(finalGoal);

        ARMARX_IMPORTANT << "Done. Pausing movement.";
        getNavigator().pause();
    }


    void
    Component::goalReached()
    {
        ARMARX_IMPORTANT << "Goal reached! (method-style)";
    }


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        def->optional(properties.robotName, "robotName");

        def->optional(properties.relativeMovement,
                      "relativeMovement",
                      "The distance between two target poses [mm]");

        return def;
    }


    ARMARX_DECOUPLED_REGISTER_COMPONENT(Component);

} // namespace armarx::navigation::components::example_client
